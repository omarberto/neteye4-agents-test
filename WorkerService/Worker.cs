using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {


                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(1000, stoppingToken);
            }
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            //TODO verificare in WIN e LINUX come console e come service
            //Microsoft.Extensions.Hosting.BackgroundService
            //verificare se usare library specifica LINUX Microsoft.Extensions.Hosting.Systemd � vantaggioso
            //verificare se usare library specifica WINDOWS Microsoft.Extensions.Hosting.WindowsServices. � vantaggioso
            return base.StartAsync(cancellationToken);
        }
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            //TODO verificare in WIN e LINUX come console e come service

            return base.StopAsync(cancellationToken);
        }
    }
}
