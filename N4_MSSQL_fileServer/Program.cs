﻿using NATS.Client;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace N4_MSSQL_fileServer
{
    class Program
    {
        internal static int instanceID = -1;
        static void Main(string[] args)
        {
            
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-i")
                {
                    if (!int.TryParse(args[++i], out instanceID))
                        throw new Exception("expeted instance ID");
                }
            }



            NATS_fileServer.Instance.Init();
            while (true)
            {
                ConsoleKeyInfo kInfo = Console.ReadKey(true);
                if (kInfo.Key == ConsoleKey.Escape)
                    break;
            }
            NATS_fileServer.Instance.Dispose();
        }

    }
}
