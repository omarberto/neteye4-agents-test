﻿using System;
using System.IO;
using NATS.Client;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Concurrent;
using System.Linq;
using System.Xml;
using System.Collections.Generic;

namespace N4_MSSQL_fileServer
{
    internal class NATS_fileServer : IDisposable
    {
        private static readonly Lazy<NATS_fileServer> lazy = new Lazy<NATS_fileServer>(() => new NATS_fileServer());
        public static NATS_fileServer Instance { get { return lazy.Value; } }

        private NATS_fileServer()
        {
            //questo si mette in ascolto e verifica cartella X
            string certificateText = File.ReadAllText(@"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\telegraf_consumer_TLSTEST.crt.pem");
            string privateKeyText = File.ReadAllText(@"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\telegraf_consumer_TLSTEST.key.pem");
            OpenSSL.X509Certificate2Provider.ICertificateProvider provider = new OpenSSL.X509Certificate2Provider.CertificateFromFileProvider(certificateText, privateKeyText, true);
            Sync_Server("neteye4-nats:4222", provider.Certificate);
        }

        public void Init()
        {

        }

        private string _serverAddress;
        private X509Certificate2 _certificate;
        private IConnection _connection = null;
        public IConnection connection
        {
            get
            {
                if (_connection == null || _connection.State == ConnState.CLOSED)
                {
                    try
                    {
                        ConnectionFactory cf = new ConnectionFactory();

                        Options opts = ConnectionFactory.GetDefaultOptions();
                        opts.Timeout = 10000;

                        opts.MaxPingsOut = 1;
                        opts.PingInterval = 120000;

                        opts.AllowReconnect = true;
                        opts.MaxReconnect = Options.ReconnectForever;
                        opts.ReconnectWait = 10000;

                        opts.AsyncErrorEventHandler = nats_AsyncErrorEventHandler;
                        opts.ClosedEventHandler = nats_ClosedEventHandler;
                        opts.DisconnectedEventHandler = nats_DisconnectedEventHandler;
                        opts.ReconnectedEventHandler = nats_ReconnectedEventHandler;

                        opts.NoRandomize = true;
                        opts.Servers = new string[] { _serverAddress };

                        if (_certificate != null)
                        {
                            opts.Secure = true;
                            opts.TLSRemoteCertificationValidationCallback = verifyServerCert;
                            opts.AddCertificate(_certificate);
                        }

                        _connection = cf.CreateConnection(opts);

                        Console.WriteLine("SERVER connected {0}", _connection.ConnectedId);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("SERVER {0}", e.Message);
                    }
                }
                return _connection;
            }
        }
        private IAsyncSubscription asyncSubscription = null;

        private bool verifyServerCert(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                return true;

            foreach (var chainElement in chain.ChainElements)
            {
                //Logging.Log.Instance.Trace(chainElement.Information);
            }

            return true;
        }
        private void Sync_Server(string serverAddress, X509Certificate2 certificate)
        {

            this._serverAddress = serverAddress;
            this._certificate = certificate;
            if (connection == null)
            {
                //qui interessante mettere un mio keepalive, visto che dopottutto devo gestire anche connessione con agente, non solo con il server
                Console.WriteLine("not connected");
            }
            else
            {
                if (asyncSubscription == null)
                {
                    //TODO put this in settings....
                    asyncSubscription = connection.SubscribeAsync("sqltrace.out.>", messageHandler);
                }
            }
            Console.WriteLine("SERVER {0} {1}", Environment.MachineName, Environment.OSVersion);
        }
        internal void messageHandler(object? sender, MsgHandlerEventArgs args)
        {
            var subjectParser = new SubjectParser(args.Message.Subject);
            Console.WriteLine("handler_Server customer instance {0} {1}", subjectParser.customer, subjectParser.instance);
            if (!receivers.ContainsKey(subjectParser.customer))
            {
                receivers.Add(subjectParser.customer, new Dictionary<string, Receiver>());
            }
            if (!receivers[subjectParser.customer].ContainsKey(subjectParser.instance))
            {
                receivers[subjectParser.customer].Add(subjectParser.instance, new Receiver(subjectParser.customer, subjectParser.instance, connection));
            }
            var res = receivers[subjectParser.customer][subjectParser.instance].messageHandler(args.Message);
            res.Wait();
            if (res.Result < 0)
            {

            }
        }

        private Dictionary<string, Dictionary<string, Receiver>> receivers = new Dictionary<string, Dictionary<string, Receiver>>();

        //TODO: test & manage following events
        private void nats_ReconnectedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("nats_ReconnectedEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        private void nats_DisconnectedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("nats_DisconnectedEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        private void nats_ClosedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("nats_ClosedEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        private void nats_AsyncErrorEventHandler(object sender, ErrEventArgs e)
        {
            Console.WriteLine("nats_AsyncErrorEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }


        public void Dispose()
        {
            var customers = receivers.Keys.ToList();
            foreach (var customer in customers)
            {
                var instances = receivers[customer].Keys.ToList();
                foreach (var instance in instances)
                {
                    receivers[customer][instance].Dispose();
                    receivers[customer].Remove(instance);
                }
                receivers.Remove(customer);
            }

            asyncSubscription.Drain();
            asyncSubscription.Dispose();

            if (_connection != null)
            {
                _connection.Drain();
                _connection.Dispose();
            }
        }
    }
}
