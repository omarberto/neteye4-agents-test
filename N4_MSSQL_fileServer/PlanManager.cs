﻿using N4_MSSQL_fileServer.remote;
using NATS.Client;
using System.IO;

namespace N4_MSSQL_fileServer
{
    internal class PlanManager : FileManager
    {

        internal PlanManager(string basePath, string customer, string instance, int fileManagerMaxQueueLength) : base(basePath, customer, instance, fileManagerMaxQueueLength, "plans")
        {

        }
        internal override string GetFileName(FileInfoExt fileInfoExt)
        {
            return Path.Combine(this._Path, fileInfoExt.Hash + ".sqlplan.zip");
        }
    }
}
