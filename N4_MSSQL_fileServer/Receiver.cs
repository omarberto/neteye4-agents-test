﻿using N4_MSSQL_fileServer.remote;
using NATS.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N4_MSSQL_fileServer
{
    internal class Receiver : IDisposable
    {
        FilesManager filesManager;
        IConnection connection;
        private enum ServerState
        {
            Unknown = 0,
            Online = 1,
            Upload = 2,
        }
        private ServerState serverState = ServerState.Unknown;
        private FileInfoExt uploadFileInfoExt = null;
        private MemoryStream memoryStream = null;

        public Receiver(string customer, string instance)
        {
            filesManager = new FilesManager(customer, instance);
        }
        public Receiver(string customer, string instance, IConnection connection):this(customer, instance)
        {
            filesManager = new FilesManager(customer, instance);
            this.connection = connection;
        }

        internal async Task<int> messageHandler(Msg NatsioMessage)
        {
            var subjectReply = NatsioMessage.Subject.Replace("sqltrace.out.", "sqltrace.in.");

            remote.ClientMessageType type;
            byte[] Message = remote.Message.GetMessage(NatsioMessage.Data, out type);
            Console.WriteLine("handler_Server message {0}", type);

            switch (serverState)
            {
                case ServerState.Upload:
                    {
                        //qui mi aspetto pacchetti, altrimenti mando in errore
                        if (type != ClientMessageType.FilePacket)
                        {
                            connection.Publish(subjectReply
                                            , new ErrorMessage(ServerErrorCode.UnvalidMessageType).Raw);
                            serverState = ServerState.Unknown;
                            return -1;
                        }
                        else if (memoryStream == null)
                        {
                            connection.Publish(subjectReply
                                            , new ErrorMessage(ServerErrorCode.UploadNotInitiated).Raw);
                            serverState = ServerState.Unknown;
                            return -1;
                        }
                        else
                        {
                            var fileBlock = FileBlock.fromBuffer(Message);

                            if (fileBlock.Hash != uploadFileInfoExt.Hash)
                            {
                                connection.Publish(subjectReply
                                            , new ErrorMessage(ServerErrorCode.UnvalidHashcode).Raw);
                                serverState = ServerState.Unknown;
                                return -1;
                            }
                            //potrei inserire altri checks sul messaggio....
                            else
                            {
                                //dovrei aggiungere un checksum per poter avere un querypacketerror, ma credo sia ridondante
                                await memoryStream.WriteAsync(fileBlock.buffer, fileBlock.Position, fileBlock.nread);


                                if (uploadFileInfoExt.Length == memoryStream.Length)
                                {
                                    filesManager.AddOrUpdateFileName(uploadFileInfoExt, memoryStream);

                                    uploadFileInfoExt = null;
                                    memoryStream = null;
                                    serverState = ServerState.Online;
                                }
                                connection.Publish(subjectReply
                                            , new FilePacketOkMessage().Raw);
                                return 0;
                                //args.Message.Respond(new remote.Message(remote.ServerMessageType.QueryPacketError).Raw);
                            }
                        }
                    }
                case ServerState.Unknown:
                case ServerState.Online:
                    {
                        switch (type)
                        {
                            case ClientMessageType.FileHeader:
                                {
                                    var fileInfoExt = FileInfoExt.fromBuffer(Message);
                                    Console.WriteLine("SendFile got responseToHeader {0}", NatsioMessage.ArrivalSubscription.Connection);

                                    if (filesManager.Exists(fileInfoExt))
                                    {
                                        //NatsioMessage.Respond(new TouchedFileMessage().Raw);
                                        connection.Publish(subjectReply
                                            , new Message(ServerMessageType.AlreadyPresentFile, new byte[] { (byte)Program.instanceID }).Raw);
                                    }
                                    else
                                    {
                                        serverState = ServerState.Upload;
                                        uploadFileInfoExt = fileInfoExt;
                                        memoryStream = new MemoryStream();
                                        //NatsioMessage.Respond(new RequestFileMessage().Raw);
                                        connection.Publish(subjectReply
                                            , new Message(ServerMessageType.RequestFile, new byte[] { (byte)Program.instanceID }).Raw);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    break;
            }
            return 0;
        }
        internal async Task<int> messageHandlerOLD(Msg NatsioMessage)
        {

            remote.ClientMessageType type;
            byte[] Message = remote.Message.GetMessage(NatsioMessage.Data, out type);
            Console.WriteLine("handler_Server message {0}", type);

            switch (serverState)
            {
                case ServerState.Upload:
                    {
                        //qui mi aspetto pacchetti, altrimenti mando in errore
                        if (type != ClientMessageType.FilePacket)
                        {
                            NatsioMessage.Respond(new ErrorMessage(ServerErrorCode.UnvalidMessageType).Raw);
                            serverState = ServerState.Unknown;
                            return -1;
                        }
                        else if (memoryStream == null)
                        {
                            NatsioMessage.Respond(new ErrorMessage(ServerErrorCode.UploadNotInitiated).Raw);
                            serverState = ServerState.Unknown;
                            return -1;
                        }
                        else
                        {
                            var fileBlock = FileBlock.fromBuffer(Message);

                            if (fileBlock.Hash != uploadFileInfoExt.Hash)
                            {
                                NatsioMessage.Respond(new ErrorMessage(ServerErrorCode.UnvalidHashcode).Raw);
                                serverState = ServerState.Unknown;
                                return -1;
                            }
                            //potrei inserire altri checks sul messaggio....
                            else
                            {
                                //dovrei aggiungere un checksum per poter avere un querypacketerror, ma credo sia ridondante
                                await memoryStream.WriteAsync(fileBlock.buffer, fileBlock.Position, fileBlock.nread);


                                if (uploadFileInfoExt.Length == memoryStream.Length)
                                {
                                    filesManager.AddOrUpdateFileName(uploadFileInfoExt, memoryStream);

                                    uploadFileInfoExt = null;
                                    memoryStream = null;
                                    serverState = ServerState.Online;
                                }
                                NatsioMessage.Respond(new FilePacketOkMessage().Raw);
                                return 0;
                                //args.Message.Respond(new remote.Message(remote.ServerMessageType.QueryPacketError).Raw);
                            }
                        }
                    }
                case ServerState.Unknown:
                case ServerState.Online:
                    {
                        switch (type)
                        {
                            case remote.ClientMessageType.QueryInfoList:
                                {
                                    Console.WriteLine("handler_Server remote.ClientMessageType.QueryInfoList");
                                    //controlla in elenco locale quali files ci sono, fa il touch
                                    //crea elenco missing files
                                    //rispondi con missing files...
                                    var fileInfoList = remote.FileInfoList.fromBuffer(Message);
                                    var missingList = new List<remote.MissingFileInfo>();
                                    foreach (var El in fileInfoList.List)
                                    {
                                        //TODO: rifare con new manager
                                        //if (!queries.ContainsKey(El.hash))
                                        //{
                                        //    missingList.Add(new remote.MissingFileInfo(El.Hash));
                                        //}
                                        Console.WriteLine("{0}: {1}", El.Name, El.LastWriteTimeUtc);
                                    }
                                    Console.WriteLine("handler_Server remote.ClientMessageType.QueryInfoList B");
                                    NatsioMessage.Respond(new remote.Message(remote.ServerMessageType.MissingQueryInfoList, new remote.MissingFileInfoList(missingList).Raw).Raw);
                                    Console.WriteLine("handler_Server remote.ClientMessageType.QueryInfoList C");
                                }
                                break;
                            case ClientMessageType.FileHeader:
                                {
                                    var fileInfoExt = FileInfoExt.fromBuffer(Message);
                                    Console.WriteLine("SendFile got responseToHeader {0}", NatsioMessage.ArrivalSubscription.Connection);

                                    if (filesManager.Exists(fileInfoExt))
                                    {
                                        //NatsioMessage.Respond(new TouchedFileMessage().Raw);
                                        NatsioMessage.Respond(new Message(ServerMessageType.AlreadyPresentFile, new byte[] { (byte)Program.instanceID }).Raw);
                                    }
                                    else
                                    {
                                        serverState = ServerState.Upload;
                                        uploadFileInfoExt = fileInfoExt;
                                        memoryStream = new MemoryStream();
                                        //NatsioMessage.Respond(new RequestFileMessage().Raw);
                                        NatsioMessage.Respond(new Message(ServerMessageType.RequestFile, new byte[] { (byte)Program.instanceID }).Raw);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    break;
            }
            return 0;
        }

        public void Dispose()
        {
        }

    }
}
