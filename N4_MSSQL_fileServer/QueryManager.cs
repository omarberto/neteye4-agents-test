﻿using N4_MSSQL_fileServer.remote;
using NATS.Client;
using System.IO;

namespace N4_MSSQL_fileServer
{
    internal class QueryManager : FileManager
    {

        internal QueryManager(string basePath, string customer, string instance, int fileManagerMaxQueueLength) : base(basePath, customer, instance, fileManagerMaxQueueLength, "queries")
        {

        }
        internal override string GetFileName(FileInfoExt fileInfoExt)
        {
            return Path.Combine(this._Path, fileInfoExt.Hash + ".sql");
        }
    }
}
