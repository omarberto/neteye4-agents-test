﻿using N4_MSSQL_fileServer.remote;
using NATS.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.IO;

namespace N4_MSSQL_fileServer
{
    internal abstract class FileManager : IFilesManager
    {
        //internal OrderedDictionary files { get; set; } = new OrderedDictionary();

        protected string _Path;
        private int fileManagerMaxQueueLength;

        protected FileManager(string basePath, string customer, string instance, int fileManagerMaxQueueLength, string subfolder)
        {
            this._Path = Path.Combine(basePath, customer, instance, subfolder);
            this.fileManagerMaxQueueLength = fileManagerMaxQueueLength;
            if (!Directory.Exists(this._Path))
                Directory.CreateDirectory(this._Path);
        }

        internal abstract string GetFileName(FileInfoExt fileInfoExt);

        public async void AddOrUpdateFileName(FileInfoExt fileInfoExt, MemoryStream memoryStream)
        {
            var fullName = GetFileName(fileInfoExt);
            using (var fileStream = File.OpenWrite(fullName))
            {
                byte[] buffer = new byte[8192];
                memoryStream.Seek(0, SeekOrigin.Begin);
                while (true)
                {
                    int nread = await memoryStream.ReadAsync(buffer, (int)memoryStream.Position, 8192);
                    if (nread > 0)
                        await fileStream.WriteAsync(buffer, (int)fileStream.Position, nread);
                    if (memoryStream.Position == memoryStream.Length)
                        break;
                }
            }
            File.SetLastWriteTimeUtc(fullName, fileInfoExt.LastWriteTimeUtc);

            ////riprisitna tutto
            //files.Add(fileInfoExt.Hash, fileInfoExt.LastWriteTimeUtc);
            //if (files.Count > fileManagerMaxQueueLength)
            //    files.RemoveAt(0);
        }

        public bool Exists(FileInfoExt fileInfoExt)
        {
            var fullName = GetFileName(fileInfoExt);

            ////riprisitna tutto
            //if (files.Contains(fileInfoExt.Hash))
            //{
            //    files.Remove(fileInfoExt.Hash);
            //    try
            //    {
            //        File.SetLastWriteTimeUtc(fullName, fileInfoExt.LastWriteTimeUtc);
            //        files.Add(fileInfoExt.Hash, fileInfoExt.LastWriteTimeUtc);
            //    }
            //    catch
            //    {
            //        return false;
            //    }
            //    return true;
            //}
            if (false) { }
            else if (File.Exists(fullName))
            {
                File.SetLastWriteTimeUtc(fullName, fileInfoExt.LastWriteTimeUtc);
                ////riprisitna tutto
                //files.Add(fileInfoExt.Hash, fileInfoExt.LastWriteTimeUtc);
                //if (files.Count > fileManagerMaxQueueLength)
                //    files.RemoveAt(0);
                return true;
            }
            else return false;
        }

        public void CleanOlder()
        {
            string[] files = Directory.GetFiles(this._Path);

            foreach (string file in files)
            {
                var fileInfo = new System.IO.FileInfo(file);
                //TODO: settings....
                if (fileInfo.LastAccessTime < DateTime.Now.AddDays(-15))
                    fileInfo.Delete();
            }
        }
    }

    internal interface IFilesManager
    {
        void AddOrUpdateFileName(FileInfoExt fileInfoExt, MemoryStream memoryStream);
        bool Exists(FileInfoExt fileInfoExt);
        void CleanOlder();
    }

    internal class FilesManager : IFilesManager
    {
        /*da configurare poi*/
        private const string basePathFormat = @"C:\Users\pb00238\Source\Repos\Agents\TEST\{0}\";
        private string basePath
        {
            get
            {
                return string.Format(basePathFormat, Program.instanceID < 0 ? "SERVER" : Program.instanceID.ToString());
            }
        }
        private const int fileManagerMaxQueueLength = 1000;
        private QueryManager queryManager;
        private PlanManager planManager;
        private DeadlockManager deadlockManager;

        public FilesManager(string customer, string instance)
        {
            queryManager = new QueryManager(basePath, customer, instance, fileManagerMaxQueueLength);
            planManager = new PlanManager(basePath, customer, instance, fileManagerMaxQueueLength);
            deadlockManager = new DeadlockManager(basePath, customer, instance, fileManagerMaxQueueLength);
        }

        public void AddOrUpdateFileName(FileInfoExt fileInfoExt, MemoryStream memoryStream)
        {
            switch (fileInfoExt.fileType)
            {
                case FileType.Query: 
                    queryManager.AddOrUpdateFileName(fileInfoExt, memoryStream);
                    return;
                case FileType.Plan:
                    planManager.AddOrUpdateFileName(fileInfoExt, memoryStream);
                    return;
                case FileType.Deadlock:
                    deadlockManager.AddOrUpdateFileName(fileInfoExt, memoryStream);
                    return;
            }
            throw new NotImplementedException(string.Format("Unknown file type {0}", fileInfoExt.fileType));
        }

        public bool Exists(FileInfoExt fileInfoExt)
        {
            switch (fileInfoExt.fileType)
            {
                case FileType.Query:
                    return queryManager.Exists(fileInfoExt);
                case FileType.Plan:
                    return planManager.Exists(fileInfoExt);
                case FileType.Deadlock:
                    return deadlockManager.Exists(fileInfoExt);
            }
            throw new NotImplementedException(string.Format("Unknown file type {0}", fileInfoExt.fileType));
        }

        public void CleanOlder()
        {
            queryManager.CleanOlder();
            planManager.CleanOlder();
            deadlockManager.CleanOlder();
        }
    }
}