﻿using System;
using System.Text;

namespace N4_MSSQL_fileServer.remote
{
    internal enum FileType : byte 
    {
        Query = 1,
        Plan = 2,
        Deadlock = 3,
    }
    internal class FileInfoExt
    {
        internal FileType fileType { get; private set; }
        internal ulong Hash { get; private set; }
        internal DateTime LastWriteTimeUtc { get; private set; }
        internal long Length { get; private set; }

        public FileInfoExt(FileType fileType, ulong Hash, DateTime LastWriteTimeUtc, long Length)
        {
            this.fileType = fileType;
            this.Hash = Hash;
            this.LastWriteTimeUtc = LastWriteTimeUtc;
            this.Length = Length;
        }

        internal static FileInfoExt fromBuffer(byte[] Raw)
        {
            FileType fileType = (FileType)Raw[0];
            ulong Hash = BitConverter.ToUInt64(Raw, 1);
            DateTime LastWriteTimeUtc = DateTime.FromBinary(BitConverter.ToInt64(Raw, 9));
            long Length = BitConverter.ToInt64(Raw, 17);
            return new FileInfoExt(fileType, Hash, LastWriteTimeUtc, Length);
        }
    }

}