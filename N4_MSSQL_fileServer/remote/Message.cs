﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace N4_MSSQL_fileServer.remote
{
    internal enum ClientMessageType : uint
    {
        QueryInfoList = 1,
        PlanInfoList = 2,

        FileHeader = 3,
        FilePacket = 4,
    }
    internal enum ServerMessageType : uint
    {
        MissingQueryInfoList = 1,
        MissingPlanInfoList = 2,

        RequestFile = 3,
        AlreadyPresentFile = 4,
        FilePacketOk = 5,
        FilePacketError = 6,
        Error = 7,
    }
    internal enum ServerErrorCode : byte
    {
        UnvalidMessageType,
        UploadNotInitiated,
        UnvalidHashcode
    }


    internal class Message
    {
        internal byte[] Raw { get; private set; }
        protected Message(ServerMessageType type)
        {
            Raw = new byte[4];
            Buffer.BlockCopy(BitConverter.GetBytes((uint)type), 0, Raw, 0, 4);
        }
        internal Message(ServerMessageType type, byte[] Payload)
        {
            Raw = new byte[4 + Payload.Length];
            Buffer.BlockCopy(BitConverter.GetBytes((uint)type), 0, Raw, 0, 4);
            Buffer.BlockCopy(Payload, 0, Raw, 4, Payload.Length);
        }

        internal static byte[] GetMessage(byte[] Raw, out ClientMessageType type)
        {
            type = (ClientMessageType)BitConverter.ToUInt32(Raw, 0);
            return Raw.Skip(4).ToArray();
        }
    }

    internal class ErrorMessage : Message
    {
        internal ErrorMessage(ServerErrorCode error) : base(ServerMessageType.Error, new byte[] { (byte)error })
        { 
        
        }
    }
    internal class RequestFileMessage : Message
    {
        internal RequestFileMessage() : base(ServerMessageType.RequestFile)
        {

        }
    }
    internal class TouchedFileMessage : Message
    {
        internal TouchedFileMessage() : base(ServerMessageType.AlreadyPresentFile)
        {

        }
    }
    internal class FilePacketOkMessage : Message
    {
        internal FilePacketOkMessage() : base(ServerMessageType.FilePacketOk)
        {

        }
    }



}
