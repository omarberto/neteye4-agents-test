﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace N4_MSSQL_fileServer.remote
{
    internal class FileBlock
    {
        internal ulong Hash { get; private set; }
        internal string Name { get { return string.Format("{0}.stmt.txt", Hash); } }
        internal int Position { get; private set; }
        internal int nread { get; private set; }
        internal byte[] buffer { get; private set; }

        public FileBlock(ulong Hash, int Position, int nread, byte[] buffer)
        {
            this.Hash = Hash;
            this.Position = Position;
            this.nread = nread;
            this.buffer = buffer;
        }
        internal static FileBlock fromBuffer(byte[] Raw)
        {
            ulong Hash = BitConverter.ToUInt64(Raw, 0);
            int packetIndex = BitConverter.ToInt32(Raw, 8);
            int nread = BitConverter.ToInt32(Raw, 12);
            byte[] buffer = new byte[nread];
            Buffer.BlockCopy(Raw, 16, buffer, 0, nread);
            return new FileBlock(Hash, packetIndex, nread, buffer);
        }
    }
}