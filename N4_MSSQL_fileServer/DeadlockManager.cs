﻿using N4_MSSQL_fileServer.remote;
using NATS.Client;
using System.IO;

namespace N4_MSSQL_fileServer
{
    class DeadlockManager : FileManager
    {

        internal DeadlockManager(string basePath, string customer, string instance, int fileManagerMaxQueueLength) : base(basePath, customer, instance, fileManagerMaxQueueLength, "deadlocks")
        {

        }
        internal override string GetFileName(FileInfoExt fileInfoExt)
        {
            string fileName = string.Format("deadlock_{0:X16}_{1:yyyyMMdd_HHmmss}.xdl", fileInfoExt.Hash, fileInfoExt.LastWriteTimeUtc);
            return Path.Combine(this._Path, fileName);
        }
    }
}
