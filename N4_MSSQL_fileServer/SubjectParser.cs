﻿namespace N4_MSSQL_fileServer
{
    internal class SubjectParser
    {
        public SubjectParser(string subject)
        {
            var els = subject.Split('.');
            if (els.Length > 4 && els[0] == "sqltrace" && els[1] == "out")
            {
                this.customer = els[2];
                for (int i = 3; i < els.Length - 1; i++)
                    this.customer += "." + els[i];
                this.instance = els[els.Length - 1];
            }
        }

        public string customer { get; internal set; }
        public string instance { get; internal set; }
    }
}