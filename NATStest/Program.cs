﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.SqlServer.XEvent.XELite;

// Reference the NATS client.
using NATS.Client;

namespace NATStest
{
    class Program
    {
        public static string ConfigPath { get; internal set; }
        static void Main(string[] args)
        {
            string path = @"C:\Users\pb00238\Installazioni\LIQUIMOLY\ANALYSIS\BATCH";
            foreach (var s in Directory.GetFiles(path, "*.xel"))
            {
                OutputXELFile(s);
            }
        }

        private static void OldMain(string[] args)
        {
            var path = @"C:\Users\pb00238\Installazioni\RHIAG\PLANS\16474117005120685646.sqlplan";
            Console.WriteLine("finfo {0}", new FileInfo(path).Name);

            bool isClient = (args.Length > 0 && args[0] == "client");

            ulong query_hash;
            if (args.Length > 1 && isClient && ulong.TryParse(args[1], out query_hash)) ;
            else query_hash = 0x17;

            if (isClient)
            {
                FileClient client = new FileClient();
                while (true)
                {
                    Console.WriteLine("press esc to terminate");
                    var rKey = Console.ReadKey(true);
                    if (rKey.Key == ConsoleKey.Escape) break;

                    client.Test();
                    //client.SendFile(query_hash);
                }

                client.Dispose();

            }
            else
            {
                FileServer server = new FileServer();
                server.Cycle();

                while (true)
                {
                    Console.WriteLine("press esc to terminate");
                    var rKey = Console.ReadKey(true);
                    if (rKey.Key == ConsoleKey.Escape) break;
                }

                server.Dispose();
            }
        }

        private static void Sync_Server()
        {
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.117:4222"
            };

            IConnection cSub = cf.CreateConnection(opts);

            ISyncSubscription s = cSub.SubscribeSync("saas.foo.queries.fileName");

            while (true)
            {
                Console.WriteLine("WAIT on message");
                try
                {
                    Msg Message = s.NextMessage(10000);
                    Console.WriteLine(Message == null);
                    Console.WriteLine(Message.ArrivalSubcription == null);
                    Console.WriteLine(Message.Data == null);
                    Console.WriteLine(Message.Reply == null);
                    Console.WriteLine(Message.Subject == null);
                    Console.WriteLine(Message.Reply);
                    Console.WriteLine(Message.Subject);
                    Message.Respond(new byte[] { });
                }
                catch (NATSTimeoutException timeout)               
                {
                    Console.WriteLine("TIMEOUT!!!!");
                }
            }

            cSub.Drain();
            cSub.Close();
        }

        private static async void Sync_Client()
        {
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.117:4222"
            };

            IConnection cSub = cf.CreateConnection(opts);
            Console.WriteLine("Request on message");
            //var Block = await cSub.RequestAsync("saas.foo.queries.fileName", BitConverter.GetBytes(17));
            //Console.WriteLine("Reply for message");
            //Console.WriteLine(Block);


            cSub.Drain();
            cSub.Close();
        }

        private static void NATSIOtest()
        {
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.117:4222"
            };

            IConnection c = cf.CreateConnection(opts);

            EventHandler<MsgHandlerEventArgs> h = (sender, args) =>
            {
                Console.WriteLine("Messaggio in arrivo {0}", args.Message);
            };

            IAsyncSubscription s = c.SubscribeAsync("saas.foo.test1", h);

            Console.WriteLine("cadcd {0} {1}", Environment.MachineName, Environment.OSVersion);

            c.Publish("saas.foo.test1", Encoding.UTF8.GetBytes(Environment.MachineName));

            while (true)
            {
                var rKey = Console.ReadKey(true);
                if (rKey.Key == ConsoleKey.Escape) break;



                c.Publish("saas.foo.test1", Encoding.UTF8.GetBytes(Environment.MachineName));
            }

            c.Drain();
            c.Close();
        }

        private static void NATSIO_Client()
        {
            Console.WriteLine("NATSIO_Client init");
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.117:4222"
            };
            
            IConnection c = cf.CreateConnection(opts);

            while (true)
            {
                Console.WriteLine("waiting for key");
                var rKey = Console.ReadKey(true);
                if (rKey.Key == ConsoleKey.Escape) break;

                if (rKey.Key == ConsoleKey.S)
                {
                    Console.WriteLine("NATSIO_Client sending");
                    SendFile(c, "16474117005120685646");
                }
            }

            c.Drain();
            c.Close();
        }

        private static async Task SendFile(IConnection c, string fileName)
        {

            var path = @"C:\Users\pb00238\Installazioni\RHIAG\PLANS\16474117005120685646.sqlplan";
            var stream = System.IO.File.OpenRead(path);

            ISyncSubscription s = c.SubscribeSync("saas.foo.queries." + fileName);

            var newQueryToSend = await c.RequestAsync("saas.foo.queries", Encoding.UTF8.GetBytes(fileName));
            var sendSubject = Encoding.UTF8.GetString(newQueryToSend.Data);
            //mettiamo un check???
            Console.WriteLine("Read sendSubject {0}", sendSubject);

            var buffer = new byte[1024];
            while (true)
            {
                try
                {
                    Console.WriteLine("Wait on requests {0}", s.PendingMessages);

                    Msg Message = s.NextMessage();

                    int blockIndex = BitConverter.ToInt32(Message.Data, 0);

                    Console.WriteLine("Got a message {0}", blockIndex);
                    // print the message
                    Console.WriteLine("{0} {1}Getting request from server for pachet {2} file {3}", Message.Subject, Message.Reply, fileName, blockIndex);

                    Console.WriteLine("file pos {0} {1}", stream.Position, stream.Length);
                    if (stream.Position >= stream.Length - 1)
                    {
                        Console.WriteLine("file end");
                        Buffer.BlockCopy(BitConverter.GetBytes(-1), 0, buffer, 0, 4);
                        Buffer.BlockCopy(BitConverter.GetBytes(0), 0, buffer, 4, 4);
                        Message.Respond(buffer);
                        break;
                    }
                    else
                    {
                        Buffer.BlockCopy(BitConverter.GetBytes(blockIndex), 0, buffer, 0, 4);
                        int nread = stream.Read(buffer, 8, buffer.Length - 8);
                        Buffer.BlockCopy(BitConverter.GetBytes(nread), 0, buffer, 4, 4);

                        Console.WriteLine("file send {0} {1}", blockIndex, nread);
                        Message.Respond(buffer);
                    }
                }
                catch(NATSTimeoutException timeout)
                {
                    Console.WriteLine("timeout!");
                    Console.WriteLine(timeout);
                }
            }

            Console.WriteLine("End of sending!!!");

            stream.Close();

            s.Drain();

            s.Dispose();
        }

        private static async void ReceiveFile(IConnection c, string fileName, string sendSubject)
        {
            System.Threading.Thread.Sleep(500);

            var path = string.Format(@"C:\Users\pb00238\Installazioni\RHIAG\PLANS\copy_{0}.sqlplan", fileName);
            var stream = System.IO.File.OpenWrite(path);

            int blockIndex = 0;
            while (true)
            {
                byte[] buffer = new byte[4];
                Buffer.BlockCopy(BitConverter.GetBytes(blockIndex), 0, buffer, 0, 4);
                try
                {
                    Console.WriteLine("Asking client {1} for packet {0}", blockIndex, sendSubject);
                    var Block = await c.RequestAsync(sendSubject, buffer);
                    // print the message
                    Console.WriteLine("Getting reply from client {0} {1}", Block.Subject, Block.Reply);
                    if (Block.Data.Length > 3)
                    {
                        int receivedBlockIndex = BitConverter.ToInt32(Block.Data, 0);
                        if (receivedBlockIndex == blockIndex)
                        {
                            if (Block.Data.Length > 7)
                            {
                                int receivedLength = BitConverter.ToInt32(Block.Data, 4);

                                if (receivedLength <= Block.Data.Length - 8)
                                {
                                    Console.WriteLine("Getting packet {0} from client len {1}", receivedBlockIndex, receivedLength);
                                    stream.Write(Block.Data, 8, receivedLength);
                                    blockIndex += receivedLength;
                                }
                            }
                        }
                        else if (receivedBlockIndex < 0)
                        {
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("message bad format");
                    }
                }
                catch(NATSTimeoutException timeoutExc)
                {
                    //vediamo come gestirlo....
                    Console.WriteLine(timeoutExc);
                    break;
                }
            }

            
            //gestire chiusura task

            stream.Close();
        }

        private static void NATSIO_Server()
        {
            Console.WriteLine("NATSIO_Server init");
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.117:4222"
            };

            IConnection c = cf.CreateConnection(opts);


            EventHandler<MsgHandlerEventArgs> h = (sender, args) =>
            {
                string fileName = Encoding.UTF8.GetString(args.Message.Data);
                // print the message
                Console.WriteLine("Getting packet from client {0} {1} {2}", args.Message.Subject, args.Message.Reply, fileName);

                args.Message.Respond(Encoding.UTF8.GetBytes("saas.foo.queries." + fileName));

                ReceiveFile(c, fileName, "saas.foo.queries." + fileName);
            };



            IAsyncSubscription s = c.SubscribeAsync("saas.foo.queries", h);

            while (true)
            {
                Console.WriteLine("waiting for key");
                var rKey = Console.ReadKey(true);
                if (rKey.Key == ConsoleKey.Escape) break;
            }

            c.Drain();
            c.Close();
        }

        private static void XELite_Test(string[] args)
        {
            string fileInput = null;
            string sessionName = null;
            string dataSource = null;
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i].ToLower();
                switch (arg)
                {

                    case "--config":
                    case "-config":
                        Program.ConfigPath = args[++i];
                        break;
                    case "--file":
                    case "-file":
                        fileInput = args[++i];
                        break;

                    case "--session":
                    case "-session":
                        sessionName = args[++i];
                        break;
                    case "--source":
                    case "-source":
                        dataSource = args[++i];
                        break;
                }
            }

            if (!string.IsNullOrEmpty(fileInput))
            {
                OutputXELFile(fileInput);
            }
            else if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(sessionName))
            {
                var sb = new System.Data.SqlClient.SqlConnectionStringBuilder();
                sb.DataSource = dataSource;
                sb.InitialCatalog = "master";
                sb.ConnectTimeout = 60;
                sb.IntegratedSecurity = true;
                sb.ApplicationIntent = System.Data.SqlClient.ApplicationIntent.ReadOnly;

                Console.WriteLine(sb.ConnectionString);
                Console.WriteLine(sessionName);

                OutputXELStream(sb.ConnectionString, sessionName);
            }
        }
        static void OutputXELFile(string fileName)
        {
            var xeStream = new XEFileEventStreamer(fileName);

            xeStream.ReadEventStream(
                () => {
                    Console.WriteLine("Headers found");
                    return Task.CompletedTask;
                },
                xevent => {

                    bool found;

                    object sql_text;
                    if (xevent.Actions.TryGetValue("sql_text", out sql_text) && Convert.ToString(sql_text).Contains("DELETE") && Convert.ToString(sql_text).Contains("BATCHJOB"))
                    {
                        Console.WriteLine("{0} {1} sql_text " + sql_text, xevent.Timestamp, fileName);
                    }
                    object statement;
                    if (xevent.Fields.TryGetValue("statement", out statement) && Convert.ToString(statement).Contains("DELETE") && Convert.ToString(statement).Contains("BATCHJOB"))
                    {
                        Console.WriteLine("{0} {1} statement " + statement, xevent.Timestamp, fileName);
                    }



                    return Task.CompletedTask;
                },
                CancellationToken.None).Wait();
        }

        static void OutputXELStream(string connectionString, string sessionName)
        {
            var cancellationTokenSource = new CancellationTokenSource();

            var xeStream = new XELiveEventStreamer(connectionString, sessionName);


            Console.WriteLine("Press any key to stop listening...");
            Task waitTask = Task.Run(() =>
            {
                Console.ReadKey();
                cancellationTokenSource.Cancel();
            });

            Task readTask = xeStream.ReadEventStream(() =>
            {
                Console.WriteLine("Connected to session");
                return Task.CompletedTask;
            },
                xevent =>
                {
                    Console.WriteLine(xevent);
                    Console.WriteLine("");
                    return Task.CompletedTask;
                },
                cancellationTokenSource.Token);


            try
            {
                Task.WaitAny(waitTask, readTask);
            }
            catch (TaskCanceledException)
            {
            }

            if (readTask.IsFaulted)
            {
                Console.Error.WriteLine("Failed with: {0}", readTask.Exception);
            }
        }
    }
}
