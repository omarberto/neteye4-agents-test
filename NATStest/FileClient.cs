﻿using NATS.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace NATStest
{
    class FileClient : IDisposable
    {
        internal FileClient()
        {

            Console.WriteLine("NATSIO_Client init");
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.5:4222"
            };

            this.connection = cf.CreateConnection(opts);
        }

        public void Dispose()
        {
            connection.Drain();
            connection.Close();
        }

        private IConnection connection;
        private string subject = "test.saas.queries";

        private class NATSMessage
        {
            internal NATSMessage(byte[] Data)
            {
                switch (Data[0])
                {
                    case 0x01:
                        this.headerMessage = new ResponseToHeader(Data);
                        break;
                    case 0x02:
                        this.fileBlockMessage = new ResponseToBlock(Data);
                        break;
                    case 0x03:
                        this.lastQueryExecutionTime = new ResponseToLastQueryExecutionTime(Data);
                        break;
                    case 0x04:
                        this.lastPlanValidCreationTime = new ResponseToLastPlanValidCreationTime(Data);
                        break;
                    default:
                        Console.WriteLine("Data[] default");
                        break;
                }
            }

            public ResponseToHeader headerMessage { get; private set; }
            public bool isHeaderMessage { get { return headerMessage != null; } }
            public ResponseToBlock fileBlockMessage { get; private set; }
            public bool isFileBlockMessage { get { return fileBlockMessage != null; } }
            public ResponseToLastQueryExecutionTime lastQueryExecutionTime { get; private set; }
            public bool isLastQueryExecutionTime { get { return lastQueryExecutionTime != null; } }
            public ResponseToLastPlanValidCreationTime lastPlanValidCreationTime { get; private set; }
            public bool isLastPlanValidCreationTime { get { return lastPlanValidCreationTime != null; } }
        }

        private class lastQueryExecutionTime
        {
            internal lastQueryExecutionTime()
            {
                this.Data = new byte[2];
                this.Data[0] = 0x03;
            }

            public byte[] Data { get; private set; }
        }
        private class ResponseToLastQueryExecutionTime
        {
            internal ResponseToLastQueryExecutionTime(byte[] Data)
            {
                this.UtcTime = DateTime.FromBinary(BitConverter.ToInt64(Data, 2));
            }

            public DateTime UtcTime { get; private set; }
        }
        private class lastPlanValidCreationTime
        {
            internal lastPlanValidCreationTime()
            {
                this.Data = new byte[2];
                this.Data[0] = 0x04;
            }

            public byte[] Data { get; private set; }
        }
        private class ResponseToLastPlanValidCreationTime
        {
            internal ResponseToLastPlanValidCreationTime(byte[] Data)
            {
                this.UtcTime = DateTime.FromBinary(BitConverter.ToInt64(Data, 2));
            }

            public DateTime UtcTime { get; private set; }
        }

        private class HeaderMessage
        {
            internal HeaderMessage(ulong query_hash, int size, DateTime lastValidExecutionTime, int type)
            {
                this.Data = new byte[26];
                this.Data[0] = 0x01;
                Buffer.BlockCopy(BitConverter.GetBytes(query_hash), 0, this.Data, 2, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(size), 0, this.Data, 10, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(lastValidExecutionTime.ToBinary()), 0, this.Data, 14, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(type), 0, this.Data, 22, 4);
            }

            public byte[] Data { get; private set; }
        }
        private class ResponseToHeader
        {
            internal ResponseToHeader(byte[] data)
            {
                this.filemissing = BitConverter.ToBoolean(data, 2);
            }

            public bool filemissing { get; private set; }
        }

        private class FileBlockMessage
        {
            internal FileBlockMessage(ulong query_hash, int type, int index, byte[] block)
            {
                this.Data = new byte[14 + block.Length];
                this.Data[0] = 0x02;
                Buffer.BlockCopy(BitConverter.GetBytes(query_hash), 0, this.Data, 2, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(type), 0, this.Data, 10, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(index), 0, this.Data, 14, 4);
                Buffer.BlockCopy(block, 0, this.Data, 18, block.Length);
            }

            public byte[] Data { get; private set; }
        }
        private class ResponseToBlock
        {
            internal ResponseToBlock(byte[] data)
            {
                this.next = BitConverter.ToBoolean(data, 2);
            }

            public bool next { get; private set; }
        }

        private async Task<bool> SendHeader(ulong query_hash, int type, int size, DateTime lastValidExecutionTime)
        {
            var headerMessage = new HeaderMessage(query_hash, size, lastValidExecutionTime, type);

            try
            {
                Msg r1 = await connection.RequestAsync(this.subject, headerMessage.Data, 1000);
                var response = new NATSMessage(r1.Data);
                if (response.isHeaderMessage)
                {
                    return response.headerMessage.filemissing;
                }
            }
            catch (NATSTimeoutException e)
            {
                Console.WriteLine("NATSTimeoutException {0}", connection.State);
            }
            return false;
        }
        private async Task<bool> SendBlock(ulong query_hash, int type, int index, byte[] block)
        {
            var blockMessage = new FileBlockMessage(query_hash, type, index, block);

            try
            {
                Msg r1 = await connection.RequestAsync(this.subject, blockMessage.Data, 1000);
                var response = new NATSMessage(r1.Data);
                if (response.isFileBlockMessage)
                {
                    Console.WriteLine(response.isFileBlockMessage);
                    return response.fileBlockMessage.next;
                }
            }
            catch (NATSTimeoutException e)
            {
                Console.WriteLine("NATSTimeoutException {0}", connection.State);
            }
            return false;
        }
        internal void SendFile(ulong query_hash)
        {


            var path = @"C:\Users\pb00238\Installazioni\RHIAG\PLANS\16474117005120685646.sqlplan";

            var finfo = new FileInfo(path);

            int type = 0;

            if (finfo.Name.Contains(".stmt"))
                type = 0x01;
            if (finfo.Name.Contains(".sqlplan"))
                type = 0x02;

            if (type == 0) return;

            var stream = System.IO.File.OpenRead(path);

            if (SendHeader(query_hash, type, (int)finfo.Length, finfo.LastWriteTimeUtc).Result)
            {
                Console.WriteLine("send blocks");

                bool next;
                int index = 0;
                byte[] buffer = new byte[256];
                //se richiesto devo mandare il file
                do
                {
                    int nRead = stream.Read(buffer, 0, buffer.Length);
                    next = SendBlock(query_hash, type, index, buffer).Result;
                    Console.WriteLine("next {0}", next);
                    index++;
                }
                while (next) ;

                stream.Close();
            }
        }


        private async Task<DateTime?> Get_lastQueryExecutionTime()
        {
            var lastQueryExecutionTimeMessage = new lastQueryExecutionTime();

            try
            {
                Msg r1 = await connection.RequestAsync(this.subject, lastQueryExecutionTimeMessage.Data, 1000);
                var response = new NATSMessage(r1.Data);
                if (response.isLastQueryExecutionTime)
                {
                    return response.lastQueryExecutionTime.UtcTime;
                }
            }
            catch (NATSTimeoutException e)
            {
                Console.WriteLine("NATSTimeoutException {0}", connection.State);
            }
            return new DateTime?();
        }
        private async Task<DateTime?> Get_lastPlanValidCreationTime()
        {
            var lastPlanValidCreationTime = new lastPlanValidCreationTime();

            try
            {
                Msg r1 = await connection.RequestAsync(this.subject, lastPlanValidCreationTime.Data, 1000);
                var response = new NATSMessage(r1.Data);
                if (response.isLastPlanValidCreationTime)
                {
                    return response.lastPlanValidCreationTime.UtcTime;
                }
            }
            catch (NATSTimeoutException e)
            {
                Console.WriteLine("NATSTimeoutException {0}", connection.State);
            }
            return new DateTime?();
        }

        internal void Test()
        {
            var res1 = Get_lastQueryExecutionTime();
            Console.WriteLine("Get_lastQueryExecutionTime {0}", res1.Result);

            var res2 = Get_lastPlanValidCreationTime();
            Console.WriteLine("Get_lastPlanValidCreationTime {0}", res2.Result);
        }
    }
}
