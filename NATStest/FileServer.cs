﻿using NATS.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace NATStest
{
    class FileServer : IDisposable
    {
        public class FileTransferInfo
        {
            public RegisteredWaitHandle Handle = null;
            public ulong hash;
            public int type;
        }
        public class FileTransferStatus
        {
            public AutoResetEvent @event;
            public int size;
            public int nread;
            internal FileStream stream;

            public bool missingBlocks { get { return nread < size; } }
            public int missingLength{ get { return size - nread; } }
        }

        internal FileServer()
        {
            status.Add(1, new Dictionary<ulong, FileTransferStatus>());
            status.Add(2, new Dictionary<ulong, FileTransferStatus>());

            Console.WriteLine("NATSIO_Server init {0}", Options.ReconnectForever);
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();

            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] {
                "nats://10.62.5.5:4222"
            };

            this.connection = cf.CreateConnection(opts);
        }


        internal void Signal(int type, ulong hash)
        {
            if (status[type].ContainsKey(hash))
                status[type][hash].@event.Set();
        }
        private void WaitProc(object state, bool timedOut)
        {
            // The state object must be cast to the correct type, because the
            // signature of the WaitOrTimerCallback delegate specifies type
            // Object.
            FileTransferInfo ti = (FileTransferInfo)state;

            if (timedOut || !status[ti.type][ti.hash].missingBlocks)
            {
                if (ti.Handle != null)
                    ti.Handle.Unregister(null);

                status[ti.type].Remove(ti.hash);
            }

            Console.WriteLine("{3:HH:mm:ss.fff} WaitProc( {0} ) executes on thread {1}; cause = {2}.",
                ti.hash,
                Thread.CurrentThread.GetHashCode().ToString(),
                timedOut ? "TIMED_OUT" : "SIGNALED",
                DateTime.Now
            );

        }

        private IConnection connection;
        private string subject = "test.saas.queries";


        private class NATSMessage
        {
            internal NATSMessage(byte[] Data)
            {
                switch (Data[0])
                {
                    case 0x01:
                        this.headerMessage = new HeaderMessage(Data);
                        break;
                    case 0x02:
                        this.fileBlockMessage = new FileBlockMessage(Data);
                        break;
                    case 0x03:
                        this.isLastQueryExecutionTimeMessage = true;
                        break;
                    case 0x04:
                        this.isLastPlanValidCreationTimeMessage = true;
                        break;
                }
            }

            public HeaderMessage headerMessage { get; private set; }
            public bool isHeaderMessage { get { return headerMessage != null; } }
            public FileBlockMessage fileBlockMessage { get; private set; }
            public bool isFileBlockMessage { get { return fileBlockMessage != null; } }
            public bool isLastQueryExecutionTimeMessage { get; private set; } = false;
            public bool isLastPlanValidCreationTimeMessage { get; private set; } = false;
        }


        private class ResponseToLastQueryExecutionTime
        {
            internal ResponseToLastQueryExecutionTime(DateTime UtcTime)
            {
                this.Data = new byte[10];
                this.Data[1] = 0x01;
                this.Data[0] = 0x03;
                Buffer.BlockCopy(BitConverter.GetBytes(UtcTime.ToBinary()), 0, this.Data, 2, 8);
            }

            public byte[] Data { get; private set; }
        }
        private class ResponseToLastPlanValidCreationTime
        {
            internal ResponseToLastPlanValidCreationTime(DateTime UtcTime)
            {
                this.Data = new byte[10];
                this.Data[1] = 0x01;
                this.Data[0] = 0x04;
                Buffer.BlockCopy(BitConverter.GetBytes(UtcTime.ToBinary()), 0, this.Data, 2, 8);
            }

            public byte[] Data { get; private set; }
        }

        private class HeaderMessage
        {
            internal HeaderMessage(byte[] Data)
            {
                this.hash = BitConverter.ToUInt64(Data, 2);
                this.size = BitConverter.ToInt32(Data, 10);
                this.lastWriteTimeUtc = DateTime.FromBinary(BitConverter.ToInt64(Data, 14));
                this.type = BitConverter.ToInt32(Data, 22);
            }

            public ulong hash { get; private set; }
            public int size { get; private set; }
            public DateTime lastWriteTimeUtc { get; private set; }
            public int type { get; private set; }
        }
        private class ResponseToHeader
        {
            internal ResponseToHeader(bool filemissing)
            {
                byte[] tmp = BitConverter.GetBytes(filemissing);
                this.Data = new byte[2 + tmp.Length];
                this.Data[1] = 0x01;
                this.Data[0] = 0x01;
                Buffer.BlockCopy(BitConverter.GetBytes(filemissing), 0, this.Data, 2, tmp.Length);
            }

            public byte[] Data { get; private set; }
        }


        private class FileBlockMessage
        {
            internal FileBlockMessage(byte[] Data)
            {
                this.query_hash = BitConverter.ToUInt64(Data, 2);
                this.type = BitConverter.ToInt32(Data, 10);
                this.index = BitConverter.ToInt32(Data, 14);
                this.block = new byte[Data.Length - 18];
                Buffer.BlockCopy(Data, 18, block, 0, block.Length);
            }

            public ulong query_hash { get; private set; }
            public int type { get; private set; }
            public int index { get; private set; }
            public byte[] block { get; private set; }
        }
        private class ResponseToBlock
        {
            internal ResponseToBlock(bool next)
            {
                this.Data = new byte[3];
                this.Data[1] = 0x01;
                this.Data[0] = 0x02;
                Buffer.BlockCopy(BitConverter.GetBytes(next), 0, this.Data, 2, 1);
            }

            public byte[] Data { get; private set; }
        }

        private Dictionary<int, Dictionary<ulong, FileTransferStatus>> status = new Dictionary<int, Dictionary<ulong, FileTransferStatus>>();
        public void Cycle()
        {
            IAsyncSubscription s = connection.SubscribeAsync(this.subject, (sender, args) =>
            {
                NATSMessage message = new NATSMessage(args.Message.Data);

                if (message.isLastQueryExecutionTimeMessage)
                {
                    ResponseToLastQueryExecutionTime response = new ResponseToLastQueryExecutionTime(DateTime.UtcNow);
                    args.Message.Respond(response.Data);
                }
                if (message.isLastPlanValidCreationTimeMessage)
                {
                    ResponseToLastPlanValidCreationTime response = new ResponseToLastPlanValidCreationTime(DateTime.UtcNow);
                    args.Message.Respond(response.Data);
                }

                if (message.isHeaderMessage)
                {
                    Console.WriteLine("Getting headerMessage from client {0} {1} {2}", message.headerMessage.hash, message.headerMessage.size, message.headerMessage.lastWriteTimeUtc);

                    //ELEMENTS:
                    var path = string.Format(@"C:\Users\pb00238\Installazioni\RHIAG\PLANS\copy_{0}.sqlplan", message.headerMessage.hash);
                    FileInfo finfo = new FileInfo(path);

                    if (finfo.Exists)
                    {
                        File.SetLastWriteTimeUtc(path, message.headerMessage.lastWriteTimeUtc);
                    }

                    ResponseToHeader response = new ResponseToHeader(finfo.Exists);
                    args.Message.Respond(response.Data);


                    if (!finfo.Exists && !status[message.headerMessage.type].ContainsKey(message.headerMessage.hash))
                    {

                        //start download
                        status[message.headerMessage.type].Add(message.headerMessage.hash, new FileTransferStatus()
                        {
                            @event = new AutoResetEvent(false),
                            size = message.headerMessage.size,
                            nread = 0,
                            stream = System.IO.File.Create(path)
                        });


                        FileTransferInfo ti = new FileTransferInfo();
                        ti.hash = message.headerMessage.hash;
                        ti.Handle = ThreadPool.RegisterWaitForSingleObject(
                            status[message.headerMessage.type][message.headerMessage.hash].@event,
                            new WaitOrTimerCallback(WaitProc),
                            ti,
                            3000,
                            false
                        );
                    }
                }
                if (message.isFileBlockMessage)
                {
                    Console.WriteLine("Getting fileBlockMessage from client {0} {1} {2}", message.fileBlockMessage.query_hash, message.fileBlockMessage.index, message.fileBlockMessage.block.Length);

                    if (status[message.fileBlockMessage.type].ContainsKey(message.fileBlockMessage.query_hash))
                    {
                        int missingLength = status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].missingLength;
                        status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].nread += message.fileBlockMessage.block.Length;
                        //Console.WriteLine("SIZE got from client {0} {1}", status[message.fileBlockMessage.query_hash].nread, status[message.fileBlockMessage.query_hash].size);

                        if (status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].missingBlocks)
                        {
                            status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].stream.Write(message.fileBlockMessage.block, 0, message.fileBlockMessage.block.Length);
                        }
                        else
                        {
                            status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].stream.Write(message.fileBlockMessage.block, 0, missingLength);
                            status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].stream.Close();
                        }
                        ResponseToBlock response = new ResponseToBlock(status[message.fileBlockMessage.type][message.fileBlockMessage.query_hash].missingBlocks);
                        args.Message.Respond(response.Data);
                        Signal(message.fileBlockMessage.type, message.fileBlockMessage.query_hash);
                    }
                }

            });
        }

        public void Dispose()
        {
            connection.Drain();
            connection.Close();
        }
    }
}
