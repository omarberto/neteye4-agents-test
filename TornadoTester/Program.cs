﻿using NATS.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TornadoTester
{
    class Program
    {
        public static string dataSource { get; set; } = "SERVERNAME:";
        public static string dataSourceTcpPort
        {
            get
            {
                int dpIndex = dataSource.IndexOf(':');
                if (dpIndex < 0)
                    return "1433";
                else
                {
                    return dataSource.Substring(1 + dpIndex);
                }
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("dataSourceTcpPort: {0}", dataSourceTcpPort);
            return;
            string certificateText = File.ReadAllText(@"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\telegraf_consumer_TLSTEST.crt.pem");
            string privateKeyText = File.ReadAllText(@"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\telegraf_consumer_TLSTEST.key.pem");
            OpenSSL.X509Certificate2Provider.ICertificateProvider provider = new OpenSSL.X509Certificate2Provider.CertificateFromFileProvider(certificateText, privateKeyText, true);
            Sync_Client("neteye4-nats:4222", provider.Certificate);
        }

        private static bool verifyServerCert(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                return true;

            foreach (var chainElement in chain.ChainElements)
            {
                //Logging.Log.Instance.Trace(chainElement.Information);
            }

            return true;
        }
        public class Account
        {
            public string Email { get; set; }
            public bool Active { get; set; }
            public DateTime CreatedDate { get; set; }
            public IList<string> Roles { get; set; }
        }

        private static async void Sync_Client(string serverAddress, X509Certificate2 certificate)
        {
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.MaxReconnect = 2;
            opts.ReconnectWait = 1000;
            opts.NoRandomize = true;
            opts.Servers = new string[] { serverAddress };

            if (certificate != null)
            {
                opts.Secure = true;
                opts.TLSRemoteCertificationValidationCallback = verifyServerCert;
                opts.AddCertificate(certificate);
            }

            //tls_ca = "C:\\TEMP\\TELEGRAF\\TEST\\root-ca.crt"
            //tls_cert = "C:\\TEMP\\TELEGRAF\\TEST\\telegraf_consumer_TLSTEST.crt.pem"
            //tls_key = "C:\\TEMP\\TELEGRAF\\TEST\\telegraf_consumer_TLSTEST.key.pem"
            //subjects = ["omar.tornado"]
            //queue_group = "wp_neteye_saas"
            //metric_buffer = 100000
            //data_format = "influx"
            //#insecure_skip_verify = false

            IConnection cSub = cf.CreateConnection(opts);

            //var Block = await cSub.RequestAsync("saas.foo.queries.fileName", BitConverter.GetBytes(17));
            //Console.WriteLine("Reply for message");
            //Console.WriteLine(Block);

            Console.WriteLine("cSub.ConnectedId {0}", cSub.ConnectedId);


            EventHandler<MsgHandlerEventArgs> h = (sender, args) =>
            {
                Console.WriteLine("Messaggio in arrivo:\n {0}", Encoding.UTF8.GetString(args.Message.Data));
            };
            cSub.SubscribeAsync("tornado_nats_json.simple_test_one", h);


            //string json = JsonConvert.SerializeObject(account, Formatting.Indented);

            while (true)
            {
                var fs = File.OpenText(@"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\keepaliveFinal.json");
                string json = fs.ReadToEnd();
                fs.Dispose();
                //Console.WriteLine(json);
                try
                {
                    //var json = string.Format(jsonFormat, "e che cazzo", "test\\\\source", 1928);
                    //Console.WriteLine(json);
                    //cSub.Publish("tornado_nats_json.simple_test_one", Encoding.UTF8.GetBytes(json));
                } 
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                    //break;
                //Console.ReadKey(true);
                System.Threading.Thread.Sleep(5000);
            }

            cSub.Drain();
            cSub.Close();
        }
    }
}
