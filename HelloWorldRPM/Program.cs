﻿using System;
using System.IO;

namespace HelloWorldRPM
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Environment.CurrentDirectory);

            Console.WriteLine("PART 1 BEGIN");
            try
            {
                foreach (var s1 in Directory.GetFiles(@"/neteye/shared/dotnet_apps"))
                {
                    Console.WriteLine("p1 directory {0}",s1);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("PART 1 END");

            Console.WriteLine("PART 2 BEGIN");
            try
            {
                foreach (var s1 in Directory.GetFiles(@"\neteye\shared\dotnet_apps"))
                {
                    Console.WriteLine("p2 directory {0}", s1);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("PART 2 END");
        }
    }
}
