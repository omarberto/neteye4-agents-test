﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace mssqlFileStorage.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class QueriesController : ControllerBase
    {
        private IConfiguration Configuration;

        public QueriesController(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }

        [HttpGet("{customer}/{query_hash}")]
        public async Task<IActionResult> Get(string customer, string query_hash)
        {
            query_hash = query_hash.Trim();
            if (string.IsNullOrEmpty(query_hash)) return null;
            if(query_hash.ToLowerInvariant().IndexOf("0x")==0) query_hash = Convert.ToUInt64(query_hash, 16).ToString();
            if (query_hash == "0") return NotFound();

            var FilesPath = Configuration["FilesPath"];
            var path = Path.Combine(FilesPath, string.Format(@"{0}", customer));
            path = Path.Combine(path, string.Format(@"{0}.sql", query_hash));
            //var path = string.Format(@"C:\Users\pb00238\Installazioni\RHIAG\PLANS\{0}.sql", query_hash);
            if(!System.IO.File.Exists(path)) return NotFound();

            var stream = System.IO.File.OpenRead(path);
            return new FileStreamResult(stream, "application/octet-stream") { FileDownloadName = query_hash + ".sql" };
        }
    }

}