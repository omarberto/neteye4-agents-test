﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace mssqlFileStorage.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlansController : ControllerBase
    {
        private IConfiguration Configuration;

        public PlansController(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }

        [HttpGet("{customer}/{queryplan_hash}")]
        public async Task<IActionResult> Get(string customer, string queryplan_hash)
        {


            queryplan_hash = queryplan_hash.Trim();
            if (string.IsNullOrEmpty(queryplan_hash)) return null;
            if(queryplan_hash.ToLowerInvariant().IndexOf("0x")==0) queryplan_hash = Convert.ToUInt64(queryplan_hash, 16).ToString();
            if (queryplan_hash == "0") return NotFound();

            var FilesPath = Configuration["FilesPath"];
            var path = Path.Combine(FilesPath, string.Format(@"{0}", customer));
            path = Path.Combine(path, string.Format(@"{0}.sqlplan", queryplan_hash));
            //var path = string.Format(@"C:\Users\pb00238\Installazioni\RHIAG\PLANS\{0}.sql", query_hash);
            if(!System.IO.File.Exists(path)) return NotFound();



            var stream = System.IO.File.OpenRead(path);
            return new FileStreamResult(stream, "application/octet-stream") { FileDownloadName = queryplan_hash + ".sqlplan" };
        }
    }

}