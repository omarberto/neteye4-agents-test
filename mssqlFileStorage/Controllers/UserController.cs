﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Linq;

namespace mssqlFileStorage.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private IConfiguration Configuration;

        public UserController(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<TodoItem>> Get(string name)
        {

            var cookies=this.Request.Cookies.Aggregate("|", (tot, el) => tot += el.Key+ "|");

            TodoItem res = new TodoItem()
            { 
                Name = this.HttpContext.User.Identity.Name,
                AuthenticationType = this.HttpContext.User.Identity.AuthenticationType,
                IsAuthenticated = this.HttpContext.User.Identity.IsAuthenticated,
                cookies = cookies
            };

            return res;
        }
    }

    public class TodoItem
    {
        public string Name { get; set; }
        public string AuthenticationType { get; set; }
        public bool IsAuthenticated { get; set; }
        public string cookies { get; set; }
    }
}