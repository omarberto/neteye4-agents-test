﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace N4_MSSQL_fileClient.remote
{
    internal struct MissingFileInfo
    {
        public MissingFileInfo(ulong Hash)
        {
            this.Hash = Hash;
        }
        public static MissingFileInfo FromBuffer(byte[] Raw, ref int Offset)
        {
            ulong Hash = BitConverter.ToUInt64(Raw, 0);
            Offset += (8);
            return new MissingFileInfo(Hash);
        }
        public static MissingFileInfo FromBuffer(byte[] Raw)
        {
            int Offset = 0;
            return FromBuffer(Raw, ref Offset);
        }

        internal ulong Hash { get; private set; }
        internal string Name { get { return string.Format("{0}.stmt.txt", Hash); } }

        public byte[] Raw
        {
            get
            {
                byte[] buffer = new byte[8];
                Buffer.BlockCopy(BitConverter.GetBytes(this.Hash), 0, buffer, 0, 8);
                return buffer;
            }
        }
    }

    internal struct MissingFileInfoList
    {
        public MissingFileInfoList(IEnumerable<MissingFileInfo> List)
        {
            this.List = List;
        }
        public static MissingFileInfoList fromBuffer(byte[] Raw)
        {
            int Length = BitConverter.ToInt32(Raw, 0);
            MissingFileInfo[] result = new MissingFileInfo[Length];
            int Offset = 4;
            for (int i = 0; i < Length; i++)
            {
                result[i] = MissingFileInfo.FromBuffer(Raw, ref Offset);
            }
            return new MissingFileInfoList(result);
        }

        public IEnumerable<MissingFileInfo> List { get; private set; }

        public byte[] Raw
        {
            get
            {
                List<byte> result = new List<byte>();
                result.AddRange(BitConverter.GetBytes(List.Count()));
                foreach (var El in List)
                    result.AddRange(El.Raw);
                return result.ToArray();
            }
        }
    }
}
