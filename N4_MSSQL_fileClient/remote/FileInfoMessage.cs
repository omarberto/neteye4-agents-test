﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace N4_MSSQL_fileClient.remote
{
    internal struct FileInfo
    {
        public FileInfo(ulong Hash, DateTime LastWriteTimeUtc)
        {
            this.Hash = Hash;
            this.LastWriteTimeUtc = LastWriteTimeUtc;
        }
        public static FileInfo FromBuffer(byte[] Raw, ref int Offset)
        {
            ulong Hash = BitConverter.ToUInt64(Raw, 0);
            DateTime LastWriteTimeUtc = DateTime.FromBinary(BitConverter.ToInt64(Raw, 8));
            Offset += 16;
            return new FileInfo(Hash, LastWriteTimeUtc);
        }
        public static FileInfo FromBuffer(byte[] Raw)
        {
            int Offset = 0;
            return FromBuffer(Raw, ref Offset);
        }

        internal ulong Hash { get; private set; }
        internal string Name { get { return string.Format("{0}.stmt.txt", Hash); } }
        public DateTime LastWriteTimeUtc { get; private set; }

        public byte[] Raw
        {
            get
            {
                byte[] buffer = new byte[16];
                Buffer.BlockCopy(BitConverter.GetBytes(this.Hash), 0, buffer, 0, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(LastWriteTimeUtc.ToBinary()), 0, buffer, 8, 8);
                return buffer;
            }
        }
    }

    internal struct FileInfoList
    {
        public FileInfoList(IEnumerable<FileInfo> List)
        {
            this.List = List;
        }
        public static FileInfoList fromBuffer(byte[] Raw)
        {
            int Length = BitConverter.ToInt32(Raw, 0);
            FileInfo[] result = new FileInfo[Length];
            int Offset = 4;
            for (int i = 0; i < Length; i++)
            {
                result[i] = FileInfo.FromBuffer(Raw, ref Offset);
            }
            return new FileInfoList(result);
        }

        public IEnumerable<FileInfo> List { get; private set; }

        public byte[] Raw
        {
            get
            {
                List<byte> result = new List<byte>();
                result.AddRange(BitConverter.GetBytes(List.Count()));
                foreach (var El in List)
                    result.AddRange(El.Raw);
                return result.ToArray();
            }
        }
    }
}
