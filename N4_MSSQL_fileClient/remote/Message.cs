﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace N4_MSSQL_fileClient.remote
{
    internal enum ClientMessageType : uint
    {
        FileList = 1, //è anche un keepalive...

        //QueryInfoList = 1,
        //PlanInfoList = 2,

        FileHeader = 3,
        FilePacket = 4,
    }
    internal enum ServerMessageType : uint
    {
        MissingQueryInfoList = 1,
        MissingPlanInfoList = 2,

        RequestFile = 3,
        AlreadyPresentFile = 4,
        FilePacketOk = 5,
        FilePacketError = 6,
        Error = 7,
    }

    internal class Message
    {
        internal byte[] Raw { get; private set; }
        internal Message(ClientMessageType type, byte[] Payload)
        {
            Raw = new byte[4 + Payload.Length];
            Buffer.BlockCopy(BitConverter.GetBytes((uint)type), 0, Raw, 0, 4);
            Buffer.BlockCopy(Payload, 0, Raw, 4, Payload.Length);
        }

        internal static byte[] GetMessage(byte[] Raw, out ServerMessageType type)
        {
            type = (ServerMessageType)BitConverter.ToUInt32(Raw, 0);
            return Raw.Skip(4).ToArray();
        }
    }
}
