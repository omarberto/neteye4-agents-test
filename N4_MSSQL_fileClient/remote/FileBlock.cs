﻿using System;
using System.Text;

namespace N4_MSSQL_fileClient.remote
{
    internal class FileBlock
    {
        internal ulong Hash { get; private set; }
        internal string Name { get { return string.Format("{0}.stmt.txt", Hash); } }
        internal int Position { get; private set; }
        internal int nread { get; private set; }
        internal byte[] buffer { get; private set; }

        public FileBlock(ulong Hash, int Position, int nread, byte[] buffer)
        {
            this.Hash = Hash;
            this.Position = Position;
            this.nread = nread;
            this.buffer = buffer;
        }

        public byte[] Raw
        {
            get
            {
                byte[] buffer = new byte[16 + this.nread];
                Buffer.BlockCopy(BitConverter.GetBytes(this.Hash), 0, buffer, 0, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(this.Position), 0, buffer, 8, 4);
                Buffer.BlockCopy(BitConverter.GetBytes(this.nread), 0, buffer, 12, 4);
                Buffer.BlockCopy(this.buffer, 0, buffer, 16, this.nread);
                return buffer;
            }
        }
    }
}