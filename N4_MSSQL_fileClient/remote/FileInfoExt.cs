﻿using System;
using System.Text;

namespace N4_MSSQL_fileClient.remote
{
    internal enum FileType : byte
    {
        Query = 1,
        Plan = 2,
        Deadlock = 3,
    }
    internal class FileInfoExt
    {
        internal FileType fileType { get; private set; }
        internal ulong Hash { get; private set; }
        internal DateTime LastWriteTimeUtc { get; private set; }
        internal long Length { get; private set; }

        public FileInfoExt(FileType fileType, ulong Hash, DateTime LastWriteTimeUtc, long Length)
        {
            this.fileType = fileType;
            this.Hash = Hash;
            this.LastWriteTimeUtc = LastWriteTimeUtc;
            this.Length = Length;
        }

        public byte[] Raw
        {
            get
            {
                byte[] buffer = new byte[25];
                buffer[0] = (byte)this.fileType;
                Buffer.BlockCopy(BitConverter.GetBytes(this.Hash), 0, buffer, 1, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(LastWriteTimeUtc.ToBinary()), 0, buffer, 9, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(Length), 0, buffer, 17, 8);
                return buffer;
            }
        }
    }
}