﻿using NATS.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace N4_MSSQL_fileClient
{
    internal class Sender : IDisposable
    {
        /*vedere se creare un sender per ogni tipo di file o no*/
        private const string filesPath = @"C:\Users\pb00238\Source\Repos\Agents\TEST\CLIENT\INSTANCE\queries";
        private string subjectOUT = "sqltrace.out.1869-wpc.local.1869DB1011\\MSSQLSERVER";

        IConnection connection;
        public Sender(IConnection connection)
        {
            this.connection = connection;
        }

        //creare classe sender ....
        private remote.FileInfoExt uploadFileInfoExt = null;
        private int packetSendTemptative = 0;
        private MemoryStream memoryStream = null;
        private byte[] sendbuffer = new byte[8192];
        private int sendnread = 0;
        public void SendFile(remote.FileType fileType, ulong Hash)
        {
            Console.WriteLine("SendFile start");

            //migliorare.....
            var fileInfo = new FileInfo(Path.Combine(filesPath, Hash + ".stmt.txt"));
            uploadFileInfoExt = new remote.FileInfoExt(fileType, Hash, fileInfo.LastWriteTimeUtc, fileInfo.Length);
            //send header
            connection.Publish(subjectOUT
                , new remote.Message(remote.ClientMessageType.FileHeader, uploadFileInfoExt.Raw).Raw);
        }
        internal async void messageHandler(object? sender, MsgHandlerEventArgs args)
        {
            Console.WriteLine("CLIENT Messaggio in arrivo {0}", args.Message.Subject);

            var fileInfo = new FileInfo(Path.Combine(filesPath, uploadFileInfoExt.Hash + ".stmt.txt"));

            remote.ServerMessageType type;
            byte[] Message = remote.Message.GetMessage(args.Message.Data, out type);
            if (type == remote.ServerMessageType.RequestFile)
            {

                Console.WriteLine("remote.ServerMessageType.RequestFile from {0}", Message[0]);
                //lettura file in memoria
                memoryStream = new MemoryStream();
                {
                    var fileStream = fileInfo.OpenRead();
                    int bufferPosition = 0;
                    while (true)
                    {
                        byte[] buffer = new byte[8192];
                        int nread = await fileStream.ReadAsync(buffer, bufferPosition, 8192);
                        if (nread > 0)
                        {
                            await memoryStream.WriteAsync(buffer, bufferPosition, nread);
                            bufferPosition += nread;
                        }
                        if (fileStream.Position == fileStream.Length)
                            break;
                    }
                }

                memoryStream.Seek(0, SeekOrigin.Begin);
                Console.WriteLine("SendFile memoryStream {0}", memoryStream.Length);

                sendnread = await memoryStream.ReadAsync(sendbuffer, (int)memoryStream.Position, 8192);
                if (sendnread > 0)
                {
                    //invio pacchetto
                    var fileBlock = new remote.FileBlock(getHash(fileInfo.Name), (int)memoryStream.Position - sendnread, sendnread, sendbuffer);

                    connection.Publish(subjectOUT,
                        new remote.Message(remote.ClientMessageType.FilePacket, fileBlock.Raw).Raw);
                }
            }
            else if (type == remote.ServerMessageType.FilePacketOk)
            {
                //invio successivo
                packetSendTemptative = 0;
                if (memoryStream.Length == memoryStream.Position)
                {
                    //TODO: close ALL
                }
                else
                {
                    sendnread = await memoryStream.ReadAsync(sendbuffer, (int)memoryStream.Position, 8192);
                    if (sendnread > 0)
                    {
                        //invio pacchetto
                        var fileBlock = new remote.FileBlock(getHash(fileInfo.Name), (int)memoryStream.Position - sendnread, sendnread, sendbuffer);

                        connection.Publish(subjectOUT,
                            new remote.Message(remote.ClientMessageType.FilePacket, fileBlock.Raw).Raw);
                    }
                }
            }
            else if (type == remote.ServerMessageType.FilePacketError)
            {
                packetSendTemptative++;
                if (packetSendTemptative == 3)
                {
                    //TODO: close ALL
                }
                else
                    //reinvio (mettiamo un massimo)
                    memoryStream.Seek(-sendnread, SeekOrigin.Current);
            }
            else if (type == remote.ServerMessageType.AlreadyPresentFile)
            {
                //fatto touch
                Console.WriteLine("remote.ServerMessageType.AlreadyPresentFile from {0}", Message[0]);
                //TODO: close ALL
            }
            else
            {
                throw new Exception("Unexpected response");
            }
        }


        public void Dispose()
        {
        }


        private static ulong getHash(string queryFileName)
        {
            string tmp = queryFileName.Replace(".stmt.txt", "");
            ulong hash = 0;
            ulong.TryParse(tmp, out hash);
            return hash;
        }

        public void SendFileInfo()
        {
            var dinfo = new DirectoryInfo(filesPath);

            var fileslist = dinfo.GetFiles().Select(finfo => new remote.FileInfo(getHash(finfo.Name), finfo.LastWriteTimeUtc));
            for (int block = 0; block < fileslist.Count(); block += 5)
            {
                Console.WriteLine("block {0}", block);
                var list = new remote.FileInfoList(fileslist.Skip(block).Take(5).Select(finfo => new remote.FileInfo(finfo.Hash, finfo.LastWriteTimeUtc)));
                var response = connection.Request("XEL_POLSKA.sqltrace.queries.server", new remote.Message(remote.ClientMessageType.QueryInfoList, list.Raw).Raw);

                remote.ServerMessageType type;
                byte[] Message = remote.Message.GetMessage(response.Data, out type);

                switch (type)
                {
                    case remote.ServerMessageType.MissingQueryInfoList:
                        {
                            Console.WriteLine("response remote.ServerMessageType.MissingQueryInfoList");
                            //controlla in elenco locale quali files ci sono, fa il touch
                            //crea elenco missing files
                            //rispondi con missing files...
                            var missingFileInfoList = remote.MissingFileInfoList.fromBuffer(Message);
                            foreach (var El in missingFileInfoList.List)
                            {
                                Console.WriteLine("{0}", El.Name);
                            }
                            Console.WriteLine("response remote.ServerMessageType.MissingQueryInfoList B");
                        }
                        break;
                }
            }
        }

        public async void SendFileOLD_RequestReply(remote.FileType fileType, ulong Hash)
        {
            int timeoutConst = 600000;
            string subject = "sqltrace.1869-wpc.local.1869DB1011\\MSSQLSERVER";

            Console.WriteLine("SendFile start");

            //migliorare.....
            var fileInfo = new FileInfo(Path.Combine(filesPath, Hash + ".stmt.txt"));
            var fileInfoExt = new remote.FileInfoExt(fileType, Hash, fileInfo.LastWriteTimeUtc, fileInfo.Length);

            try
            {
                //send header
                var responseToHeader = connection.Request(subject
                    , new remote.Message(remote.ClientMessageType.FileHeader, fileInfoExt.Raw).Raw, timeoutConst);

                remote.ServerMessageType type;
                byte[] Message = remote.Message.GetMessage(responseToHeader.Data, out type);

                //il meccanismo seguente serve ad usare il sendfile per tutte le queries 
                if (type == remote.ServerMessageType.RequestFile)
                {

                    Console.WriteLine("remote.ServerMessageType.RequestFile from {0}", Message[0]);
                    //lettura file in memoria
                    MemoryStream memoryStream = new MemoryStream();
                    {
                        var fileStream = fileInfo.OpenRead();
                        byte[] buffer = new byte[8192];
                        int bufferPosition = 0;
                        while (true)
                        {
                            int nread = await fileStream.ReadAsync(buffer, bufferPosition, 8192);
                            if (nread > 0)
                            {
                                await memoryStream.WriteAsync(buffer, bufferPosition, nread);
                                bufferPosition += nread;
                            }
                            if (fileStream.Position == fileStream.Length)
                                break;
                        }
                    }

                    memoryStream.Seek(0, SeekOrigin.Begin);
                    Console.WriteLine("SendFile memoryStream {0}", memoryStream.Length);

                    {
                        int packetSendTemptative = 0;
                        byte[] buffer = new byte[8192];
                        while (true)
                        {
                            int nread = await memoryStream.ReadAsync(buffer, (int)memoryStream.Position, 8192);
                            if (nread > 0)
                            {
                                //invio pacchetto
                                var fileBlock = new remote.FileBlock(getHash(fileInfo.Name), (int)memoryStream.Position - nread, nread, buffer);
                                var responseToPacket = connection.Request(subject,
                                    new remote.Message(remote.ClientMessageType.FilePacket, fileBlock.Raw).Raw, timeoutConst);
                                //ricezione ok invio pacchetto

                                byte[] responseToPacketMessage = remote.Message.GetMessage(responseToPacket.Data, out type);
                                if (type == remote.ServerMessageType.FilePacketOk)
                                {
                                    //invio successivo
                                    packetSendTemptative = 0;
                                    if (memoryStream.Length == memoryStream.Position)
                                        break;
                                }
                                else if (type == remote.ServerMessageType.FilePacketError)
                                {
                                    packetSendTemptative++;
                                    if (packetSendTemptative == 3)
                                        break;
                                    //reinvio (mettiamo un massimo)
                                    memoryStream.Seek(-nread, SeekOrigin.Current);
                                }
                                else
                                {
                                    throw new Exception("Unexpected response");
                                }
                            }
                        }
                    }
                    //
                }
                else if (type == remote.ServerMessageType.AlreadyPresentFile)
                {
                    //fatto touch
                    Console.WriteLine("remote.ServerMessageType.AlreadyPresentFile from {0}", Message[0]);
                }
                else
                {
                    throw new Exception("Unexpected response");
                }
            }
            catch (NATS.Client.NATSTimeoutException e)
            {
                Console.WriteLine("NATS.Client.NATSTimeoutException");
            }
        }
    }
}
