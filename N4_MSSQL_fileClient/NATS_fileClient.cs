﻿using System;
using System.IO;
using NATS.Client;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using System.Xml;

namespace N4_MSSQL_fileClient
{
    internal class NATS_fileClient : IDisposable
    {
        private static readonly Lazy<NATS_fileClient> lazy = new Lazy<NATS_fileClient>(() => new NATS_fileClient());
        public static NATS_fileClient Instance { get { return lazy.Value; } }
        private NATS_fileClient()
        {
        }

        public void Init(string server, string certificate)
        {
            //questo si mette in ascolto e verifica cartella X
            X509Certificate2 cert;
            {
                cert = new X509Certificate2(certificate, "password");
            }

            Sync_Client(string.IsNullOrEmpty(server) ? "neteye4-nats:4222" : server, cert);
        }

        private Sender sender;
        internal void SendFile()
        {
            sender.SendFile(remote.FileType.Query, 10221957899016054418);
        }

        public void Init(string server, string certificate, string privateKey)
        {
            //questo si mette in ascolto e verifica cartella X
            X509Certificate2 cert;

            {
                string certificateText = File.ReadAllText(string.IsNullOrEmpty(certificate) ? @"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\telegraf_consumer_TLSTEST.crt.pem" : certificate);
                string privateKeyText = File.ReadAllText(string.IsNullOrEmpty(privateKey) ? @"C:\Users\pb00238\Source\Repos\Agents\TornadoTester\telegraf_consumer_TLSTEST.key.pem" : privateKey);


                OpenSSL.X509Certificate2Provider.ICertificateProvider provider = new OpenSSL.X509Certificate2Provider.CertificateFromFileProvider(certificateText, privateKeyText, true);//, true

                cert = provider.Certificate;
            }

            Sync_Client(string.IsNullOrEmpty(server) ? "neteye4-nats:4222" : server, cert);
        }

        private IConnection connection;

        private bool verifyClientCert(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                return true;

            foreach (var chainElement in chain.ChainElements)
            {
                //Logging.Log.Instance.Trace(chainElement.Information);
            }

            return true;
        }
        private IAsyncSubscription asyncSubscription = null;
        private string subjectIN = "sqltrace.in.1869-wpc.local.1869DB1011\\MSSQLSERVER";
        private void Sync_Client(string serverAddress, X509Certificate2 certificate)
        {
            ConnectionFactory cf = new ConnectionFactory();

            Options opts = ConnectionFactory.GetDefaultOptions();
            opts.AllowReconnect = true;
            opts.MaxReconnect = Options.ReconnectForever;
            opts.ReconnectWait = 1000;

            opts.AsyncErrorEventHandler = nats_AsyncErrorEventHandler;
            opts.ClosedEventHandler = nats_ClosedEventHandler;
            opts.DisconnectedEventHandler = nats_DisconnectedEventHandler;
            opts.ReconnectedEventHandler = nats_ReconnectedEventHandler;

            opts.NoRandomize = true;
            opts.Servers = new string[] { serverAddress };

            if (certificate != null)
            {
                opts.Secure = true;
                opts.TLSRemoteCertificationValidationCallback = verifyClientCert;
                opts.AddCertificate(certificate);
            }

            Console.WriteLine("CLIENT {0} ready for messaging", serverAddress);

            connection = cf.CreateConnection(opts);

            sender = new Sender(connection);
            if (asyncSubscription == null)
            {
                //TODO put this in settings....
                asyncSubscription = connection.SubscribeAsync(subjectIN, sender.messageHandler);
            }
        }
        //TODO: test & manage following events
        private void nats_ReconnectedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("nats_ReconnectedEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        private void nats_DisconnectedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("nats_DisconnectedEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        private void nats_ClosedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("nats_ClosedEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        private void nats_AsyncErrorEventHandler(object sender, ErrEventArgs e)
        {
            Console.WriteLine("nats_AsyncErrorEventHandler {0} {1:HH:mm:ss.fff}", e.Conn.ConnectedId, DateTime.Now);
            //throw new NotImplementedException();
        }

        public void Dispose()
        {
            connection.Drain();
            connection.Dispose();
        }
    }
}
