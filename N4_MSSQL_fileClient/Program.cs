﻿using NATS.Client;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace N4_MSSQL_fileClient
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args != null && args.Length==4)
                NATS_fileClient.Instance.Init(args[0], args[1], args[2]);
            else if (args != null && args.Length == 3)
                NATS_fileClient.Instance.Init(args[0], args[1]);
            else
                NATS_fileClient.Instance.Init(null, null, null);

            while (true)
            {
                ConsoleKeyInfo kInfo = Console.ReadKey(true);
                NATS_fileClient.Instance.SendFile();
                if (kInfo.Key == ConsoleKey.Escape)
                    break;
            }
            NATS_fileClient.Instance.Dispose();
        }
    }
}
