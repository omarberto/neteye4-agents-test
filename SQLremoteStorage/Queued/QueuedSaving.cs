﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SQLremoteStorage.Queued
{
    public enum FileSaveActionType
    {
        Query,
        QueryTouch,
        Plan,
        PlanTouch,
        CleanOld,
    }
    public class FileSaveAction
    {
        public FileSaveAction(FileSaveActionType type, Action action, params object[] parameters)
        {
            this.type = type;
            this.action = action;
            this.parameters = parameters;
        }

        public FileSaveActionType type { get; private set; }
        public Action action { get; private set; }
        public object[] parameters { get; private set; }
    }

    public sealed class QueueAppendable : IDisposable
    {
        private static readonly Lazy<QueueAppendable> lazy = new Lazy<QueueAppendable>(() => new QueueAppendable());

        public static QueueAppendable Instance { get { return lazy.Value; } }

        public int CurrentlyQueuedTasks { get { return _Queue.Count; } }

        BlockingCollection<FileSaveAction> _Queue = new BlockingCollection<FileSaveAction>();
        private QueueAppendable()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Console.WriteLine("QueueAppendable _Queue.TryTake() start");
                    
                    try
                    {
                        FileSaveAction action;
                        bool hasAction = _Queue.TryTake(out action, 300000);
                        if (hasAction)
                        {
                            action.action();

                            switch (action.type)
                            {
                                case FileSaveActionType.CleanOld:
                                    SQLremoteStorage.Storage.Instance.CleanOld();
                                    break;
                                case FileSaveActionType.Query:
                                    Console.WriteLine("Query save 0x{0:X16} 0x{1:X16} {2:yyMMddHHmmss}", action.parameters[0], action.parameters[1], action.parameters[2]);
                                    break;
                                case FileSaveActionType.QueryTouch:
                                    Console.WriteLine("Query touch 0x{0:X16} {1:yyMMddHHmmss}", action.parameters[0], action.parameters[1]);
                                    break;
                                case FileSaveActionType.Plan:
                                    Console.WriteLine("Plan save 0x{0:X16} 0x{1:X16}", action.parameters[0], action.parameters[1]);
                                    break;
                                case FileSaveActionType.PlanTouch:
                                    Console.WriteLine("Plan touch 0x{0:X16} {1:yyMMddHHmmss}", action.parameters[0], action.parameters[1]);
                                    break;
                            }
                        }

                        Console.WriteLine("QueueAppendable _Queue.TryTake() ended with result {0}", hasAction);
                    }
                    catch (InvalidOperationException e)
                    {
                        Logging.Log.Instance.Error(e, "QuerySave.QueueAppendable InvalidOperationException");
                        break;
                    }
                    catch (Exception e)
                    {
                        Logging.Log.Instance.Error(e, "QuerySave.QueueAppendable Exception");
                        // TODO log me
                    }

                    SQLremoteStorage.Storage.Instance.CleanOld();
                }
            });
        }
        public void Append(FileSaveAction action)
        {
            _Queue.Add(action);
        }

        public void Dispose()
        {
            _Queue.CompleteAdding();
        }
    }
}
