﻿
using Nett;
using System.Collections.Generic;

namespace SQLremoteStorage.Config
{
    public class Configuration
    {
        private static Configuration _config;
        public static Configuration config
        {
            get
            {
                return _config;
            }
        }
        public static void Init(string configPath)
        {
            _config = Toml.ReadFile<Configuration>(configPath);
        }
        public static bool Init(string configPath, string accountName)
        {
            _config = Toml.ReadFile<Configuration>(configPath);

            if (config.hasStorage)
            {
                if (config.storage.filesystemActive)
                {
                    try
                    {
                        config.storage.fileSystem.Init(accountName);
                    }
                    catch (System.Exception e)
                    {
                        //Logging.Log.Instance.Info(e, "config.storage.fileSystem.Init() exception");
                        return false;
                    }
                }
            }
            return true;
        }

        public StorageConfiguration storage { get; set; }
        [Nett.TomlIgnore]
        public bool hasStorage
        {
            get
            {
                //per ora ne ho uno e basta... (di base è privilegiato nats)
                return storage != null;
            }
        }

        public static bool Test(string configfile, out List<string> warnings, out List<string> errors)
        {
            Init(configfile);
            //

            warnings = new List<string>();
            errors = new List<string>();

            if (config.hasStorage)
            {
                //accettabile
                config.storage.Test(ref warnings, ref errors);
            }
            else
            {
                //non accettabile?????
                errors.Add("main.storage configuration is missing");
            }
            
            return errors.Count == 0;
        }
    }
}
