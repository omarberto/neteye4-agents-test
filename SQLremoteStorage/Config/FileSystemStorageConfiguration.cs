﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;

namespace SQLremoteStorage.Config
{
    public class FileSystemStorageConfiguration
    {
        public bool zipped { set; get; } = true;

        public string queriespath { set; get; }

        public string planspath { set; get; }

        //path autoimatiche eliminate

        internal void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(queriespath))
            {
                //non accettabile
                errors.Add("main.storage.fileSystem.queriespath configuration is missing");
            }
            else
            {
                //accettabile
                if (Directory.Exists(queriespath))
                {

                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", queriespath));
                }
            }

            if (string.IsNullOrEmpty(planspath))
            {
                //non accettabile
                errors.Add("main.storage.fileSystem.planspath configuration is missing");
            }
            else
            {
                //accettabile
                if (Directory.Exists(planspath))
                {
                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", queriespath));
                }
            }
        }

        internal void Init(string accountName)
        {
            if (!Directory.Exists(queriespath))
            {
                Directory.CreateDirectory(queriespath);//parametrizzare, generalizzare etc....
            }

            if (!Directory.Exists(planspath))
            {
                Directory.CreateDirectory(planspath);//parametrizzare, generalizzare etc....
            }
        }
    }
}