﻿namespace SQLremoteStorage.Config
{
    public class DataStorageConfiguration
    {
        public FileSystemStorageConfiguration fileSystem { get; set; }
        public bool filesystemActive
        {
            get
            {
                return fileSystem != null;
            }
        }
    }
}