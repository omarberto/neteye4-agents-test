﻿using System.Collections.Generic;

namespace SQLremoteStorage.Config
{
    public class StorageConfiguration
    {
        public FileSystemStorageConfiguration fileSystem { get; set; }
        [Nett.TomlIgnore]
        public bool filesystemActive
        {
            get
            {
                return fileSystem != null;
            }
        }


        internal void Test(ref List<string> warnings, ref List<string> errors)
        {

            if (filesystemActive)
            {
                //accettabile
                fileSystem.Test(ref warnings, ref errors);
            }
            else
            {
                //non accettabile
                errors.Add("main.storage.fileSystem configuration is missing");
            }
        }
    }
}