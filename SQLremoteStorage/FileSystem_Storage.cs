﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLremoteStorage
{
    internal class FileSystem_Storage : IStorage
    {
        private class QueryTimeStats
        {
            //questa la posso aggiornare ogni volta
            //last validExecutionTime
            private DateTime lastExecutionTime;
            public DateTime lastValidExecutionTime;

            public QueryTimeStats(DateTime lastValidExecutionTime)
            {
                //all'inizio sono allineati
                this.lastExecutionTime = this.lastValidExecutionTime = lastValidExecutionTime;
            }

            public bool UpdateAndVerifyTime(DateTime lastExecutionTime)
            {
                this.lastExecutionTime = lastExecutionTime;
                bool toUpdate = ((lastExecutionTime - this.lastValidExecutionTime).TotalSeconds > 60);
                if (toUpdate) this.lastValidExecutionTime = lastExecutionTime;
                return toUpdate;
            }
        }
        private class PlanTimeStats
        {
            //questa la posso aggiornare ogni volta
            //last validExecutionTime
            private DateTime lastExecutionTime;
            public DateTime lastValidCreationTime;

            public PlanTimeStats(DateTime lastValidCreationTime)
            {
                //all'inizio sono allineati
                this.lastExecutionTime = this.lastValidCreationTime = lastValidCreationTime;
            }

            public bool UpdateAndVerifyTime(DateTime lastExecutionTime)
            {
                this.lastExecutionTime = lastExecutionTime;
                return ((lastExecutionTime - this.lastValidCreationTime).TotalSeconds > 300);
            }
            public bool UpdateAndVerifyCreationTime(DateTime lastCreationTime)
            {
                this.lastValidCreationTime = lastCreationTime;
                return lastCreationTime > this.lastValidCreationTime;
            }
        }

        private ConcurrentDictionary<ulong, QueryTimeStats> queries;
        private ConcurrentDictionary<ulong, PlanTimeStats> plans;
        private string queriesPath;
        private string plansPath;

        public FileSystem_Storage()
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            this.queriesPath = Config.Configuration.config.storage.fileSystem.queriespath;
            this.plansPath = Config.Configuration.config.storage.fileSystem.planspath;
            if (!Directory.Exists(queriesPath)) Directory.CreateDirectory(queriesPath);//parametrizzare, generalizzare etc....
            if (!Directory.Exists(plansPath)) Directory.CreateDirectory(plansPath);//parametrizzare, generalizzare etc....
            Console.Write("started collecting queries info ... ");
            this.queries = new ConcurrentDictionary<ulong, QueryTimeStats>(Directory.GetFiles(this.queriesPath, "*.stmt.txt").Select(file => new { hash = ulong.Parse(Path.GetFileName(file).Replace(".stmt.txt", "")), time = File.GetLastWriteTime(file) }).ToDictionary(obj => obj.hash, obj => new QueryTimeStats(obj.time)));
            Console.WriteLine("done, got {0}", this.queries.Count);
            Console.Write("started collecting queries info ... ");
            string planextension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";
            this.plans = new ConcurrentDictionary<ulong, PlanTimeStats>(Directory.GetFiles(this.plansPath, "*" + planextension).Select(file => new { hash = ulong.Parse(Path.GetFileName(file).Replace(planextension, "")), time = File.GetLastWriteTime(file) }).ToDictionary(obj => obj.hash, obj => new PlanTimeStats(obj.time)));
            Console.WriteLine("done, got {0}", this.plans.Count);


            SQLremoteStorage.Queued.QueueAppendable.Instance.Append(new SQLremoteStorage.Queued.FileSaveAction(
                SQLremoteStorage.Queued.FileSaveActionType.CleanOld,
                () => CleanOld()));


        }


        public void CleanOld()
        {
            CleanOldQueries();
            CleanOldPlans();
        }

        private void CleanOldQueries()
        {
            Console.WriteLine("CleanOldQueries");
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            var filesToRemove = this.queries.Where(el => (DateTime.Now - el.Value.lastValidExecutionTime).TotalDays > 15).Select(el => el.Key);
            sw.Stop();
            Console.WriteLine("CleanOldQueries {0} {1}", filesToRemove.Count(), sw.ElapsedMilliseconds);
            sw.Restart();
            foreach (var query_hash in filesToRemove)
            {
                try
                {
                    Logging.Log.Instance.Debug("Deleting  file for {0}.stmt.txt", query_hash);
                    QueryTimeStats tmp;
                    File.Delete(Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)));
                    bool res = queries.TryRemove(query_hash, out tmp);
                    Logging.Log.Instance.Debug("Deleted {2} file for {0}.stmt.txt {1}", query_hash, tmp.lastValidExecutionTime, res);
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "Exception deleting query {0}.stmt.txt", query_hash);
                }
            }
            sw.Stop();
            Console.WriteLine("CleanedOldQueries {0} {1}", filesToRemove.Count(), sw.ElapsedMilliseconds);
        }
        private void CleanOldPlans()
        {
            string extension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";

            Console.WriteLine("CleanOldPlans");
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            var filesToRemove = this.plans.Where(el => (DateTime.Now - el.Value.lastValidCreationTime).TotalDays > 15).Select(el => el.Key);
            sw.Stop();
            Console.WriteLine("CleanOldPlans {0} {1}", filesToRemove.Count(), sw.ElapsedMilliseconds);
            sw.Restart();

            foreach (var queryplan_hash in filesToRemove)
            {
                try
                {
                    Logging.Log.Instance.Debug("Deleting  file for {0}{1}", queryplan_hash, extension);
                    PlanTimeStats tmp;
                    File.Delete(Path.Combine(plansPath, string.Format("{0}{1}", queryplan_hash, extension)));
                    bool res = plans.TryRemove(queryplan_hash, out tmp);
                    Logging.Log.Instance.Debug("Deleted {2} file for {0}.sqlplan {1}", queryplan_hash, tmp.lastValidCreationTime, res);
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "Exception deleting query {0}.sqlplan", queryplan_hash);
                }
            }

            sw.Stop();
            Console.WriteLine("CleanedOldPlans {0} {1}", filesToRemove.Count(), sw.ElapsedMilliseconds);
        }

        internal bool HasQuery(ulong query_hash)
        {
            Logging.Log.Instance.Debug("HasQuery {0}", query_hash);
            return queries.ContainsKey(query_hash);
        }
        internal bool HasQuery(ulong query_hash, DateTime lastExecutionTime)
        {
            Logging.Log.Instance.Info("HasQuery {0} - {1}", query_hash, lastExecutionTime);
            bool exists = queries.ContainsKey(query_hash);
            if (exists && queries[query_hash].UpdateAndVerifyTime(lastExecutionTime))
            {
                SQLremoteStorage.Queued.QueueAppendable.Instance.Append(new SQLremoteStorage.Queued.FileSaveAction(
                    SQLremoteStorage.Queued.FileSaveActionType.QueryTouch,
                    () => QueryTouch(query_hash, lastExecutionTime),
                    query_hash, lastExecutionTime));

            }
            return exists;
        }

        private void QueryTouch(ulong query_hash, DateTime lastExecutionTime)
        {
            Logging.Log.Instance.Info("HasQuery {2} updating {0} - {1}", query_hash, lastExecutionTime, Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)));
            try
            {
                System.IO.File.SetLastWriteTime(Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)), lastExecutionTime);
                Logging.Log.Instance.Info("HasQuery {2} updated {0} - {1}", query_hash, lastExecutionTime, Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)));
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Error(e, "IOException touching query {0}.stmt.txt", query_hash);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error(e, "other Exception touching query {0}.stmt.txt", query_hash);
            }
        }

        public void InsertQuery(ulong query_hash, string sql_text, DateTime lastExecutionTime)
        {
            var filepath = Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash));
            if (File.Exists(filepath)) return;

            try
            {
                using (var sourceStream = new StreamWriter(new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Read, bufferSize: 4096, useAsync: true), Encoding.Unicode))
                {
                    sourceStream.WriteAsync(sql_text).Wait();
                }
                System.IO.File.SetLastWriteTime(filepath, lastExecutionTime);

                queries.TryAdd(query_hash, new QueryTimeStats(lastExecutionTime));
            }
            catch//(System.IO.IOException e)
            {

            }
        }

        public bool HasPlan(ulong query_plan_hash)
        {
            return plans.ContainsKey(query_plan_hash);
        }
        public bool HasPlanWithValidTime(ulong query_plan_hash, DateTime lastExecutionTime)
        {
            Logging.Log.Instance.Info("HasPlan {0} - {1}", query_plan_hash, lastExecutionTime);
            bool exists = plans.ContainsKey(query_plan_hash);
            return (exists && plans[query_plan_hash].UpdateAndVerifyTime(lastExecutionTime));
        }
        public bool HasPlan(ulong query_plan_hash, DateTime creationTime)
        {
            string extension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";

            Logging.Log.Instance.Info("HasPlan {0} - {1}", query_plan_hash, creationTime);
            bool exists = plans.ContainsKey(query_plan_hash);
            if (exists && plans[query_plan_hash].UpdateAndVerifyCreationTime(creationTime))
            {
                SQLremoteStorage.Queued.QueueAppendable.Instance.Append(new SQLremoteStorage.Queued.FileSaveAction(
                    SQLremoteStorage.Queued.FileSaveActionType.PlanTouch,
                    () => PlanTouch(query_plan_hash, creationTime, extension),
                    query_plan_hash, creationTime, extension));
            }
            return exists;
        }

        private void PlanTouch(ulong query_plan_hash, DateTime creationTime, string extension)
        {
            Logging.Log.Instance.Info("HasPlan {2} updating {0} - {1}", query_plan_hash, creationTime, Path.Combine(queriesPath, string.Format("{0}{1}", query_plan_hash, extension)));
            try
            {
                System.IO.File.SetLastWriteTime(Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)), creationTime);
                Logging.Log.Instance.Info("HasPlan {2} updated {0} - {1}", query_plan_hash, creationTime, Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)));
                var reso = System.IO.File.GetLastWriteTime(Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)));
                Logging.Log.Instance.Info("HasPlan {2} updated resulkt {0} - {1}", query_plan_hash, reso, Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)));
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Error(e, "IOException touching plan {0}.sqlplan", query_plan_hash);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error(e, "other Exception touching plan {0}.sqlplan", query_plan_hash);
            }
        }

        public void InsertPlan(ulong query_plan_hash, string query_plan, DateTime creationTime)
        {
            bool res = false;
            if (Config.Configuration.config.storage.fileSystem.zipped)
            {
                //Console.WriteLine("adding plan {0}.sqlplan.zip length {1}", query_plan_hash, string.IsNullOrEmpty(query_plan));
                res = InsertPlanZipped(query_plan_hash, query_plan);
            }
            else
                res = InsertPlanText(query_plan_hash, query_plan);

            if (res)
                plans.TryAdd(query_plan_hash, new PlanTimeStats(creationTime));
        }
        private bool InsertPlanText(ulong query_plan_hash, string query_plan)
        {
            Logging.Log.Instance.Trace("adding plan {0}.sqlplan", query_plan_hash);
            var filepath = Path.Combine(plansPath, string.Format("{0}.sqlplan", query_plan_hash));
            if (File.Exists(filepath)) return true;

            try
            {
                using (var sourceStream = new StreamWriter(new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Read, bufferSize: 4096, useAsync: true), Encoding.Unicode))
                {
                    sourceStream.WriteAsync(query_plan).Wait();
                    Logging.Log.Instance.Trace("write plan end");
                }

                return true;
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Debug(e, "IOException {0}", filepath);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Debug(e, "Exception {0}", filepath);
            }

            return false;
        }
        private bool InsertPlanZipped(ulong query_plan_hash, string query_plan)
        {
            Logging.Log.Instance.Trace("adding plan {0}.sqlplan.zip length {1}", query_plan_hash, query_plan.Length);
            var filepath = Path.Combine(plansPath, string.Format("{0}.sqlplan.zip", query_plan_hash));
            if (File.Exists(filepath)) return true;

            try
            {
                using (FileStream zipToOpen = new FileStream(filepath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read, bufferSize: 4096, useAsync: true))
                {
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                    {
                        var planEntry = archive.CreateEntry(string.Format("{0}.sqlplan", query_plan_hash));
                        using (StreamWriter writer = new StreamWriter(planEntry.Open(), Encoding.Unicode))
                        {
                            writer.WriteAsync(query_plan).Wait();
                        }
                    }
                }

                Logging.Log.Instance.Trace("write plan end");

                return true;
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Error(e, "IOException {0}", filepath);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error(e, "Exception {0}", filepath);
            }

            return false;
        }
    }
}
