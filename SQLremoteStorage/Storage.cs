﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLremoteStorage
{

    public sealed class Storage
    {
        private static readonly Lazy<IStorage> lazy = new Lazy<IStorage>(() => Init());

        private static IStorage Init()
        {
            if (Config.Configuration.config.storage.filesystemActive)
                return (IStorage)new FileSystem_Storage();

            return (IStorage)new Mock_Storage();
        }

        public static IStorage Instance { get { return lazy.Value; } }

        private Storage()
        {

        }

    }
}
