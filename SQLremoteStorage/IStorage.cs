﻿using System;

namespace SQLremoteStorage
{
    public interface IStorage
    {
        void InsertPlan(ulong query_plan_hash, string query_plan, DateTime creationTime);
        void InsertQuery(ulong query_hash, string sql_text, DateTime creationTime);
        void CleanOld();
    }
}